<?php
class _CheckoutPage
{
    static $PopupSelector='#om-pxoik9nvnicf0x0yiduk-optin > div > button > svg > path';
    static $billingEmail_fieldSelector='#billing_email';
    static $BuynowButton="button[name='add-to-cart']";
    static $Stripe_CreditcardButton='#payment_method_stripe';
    static $checkout_PlaceOrderButton="button[name='woocommerce_checkout_place_order']";
    static $Confirm_PayText='Confirm and Pay';
    static $Place_OrderButton='#stripe-payment-data > button';
    static $IframeSelector='stripe_checkout_app';
    static $PayNowButton='#container > section > span:nth-child(3) > div > div > main > form > nav > div > div > div > button';

 Static $Wp_answer_BuyNow='.buy-now-bttuon.elementor-widget.elementor-widget-shortcode > div > div > div > a';

static $SurveyBuyNowHeaderButton="#menu-item-4443 > a";

static $SurveyBuyNowButton1="#intro > div.col.span_12.light.left > div > div > div > a > span";
static  $SurveyBuyNowButton2="#pricing > div.col.span_12.dark.left > div > div > div > div.wpb_row.vc_row-fluid.vc_row.standard_section.license_details > div.col.span_12.center > div > div > div > a > span";
static $ScrollToBuyNowSelector='#pricing > div.col.span_12.dark.left > div > div > div > div.wpb_row.vc_row-fluid.vc_row.standard_section.license_details > div.col.span_12.center > div > div > div > div:nth-child(1) > h3';
static $CardNumberField="input[name='cardnumber']";
static $ExpDateField="input[name='exp-date']";
static $CVVNumberField="input[name='cvc']";


static $CarNuberiframeSelector='#stripe-card-element > div > iframe';
static $ExpDateiframeSelector='#stripe-exp-element > div > iframe';
static $CvvNumberiframeSelector='#stripe-cvc-element > div > iframe';
static $RemoveButton="//a[@class='remove']";

}

 ?>
