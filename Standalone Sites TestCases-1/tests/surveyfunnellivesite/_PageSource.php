<?php
class _PageSource
{
    static $metaViewportXPath               = 'head > meta[name="viewport"]';
    //static $metaViewportXPath               = '/html/head/meta[2]';
    static $desiredMetaViewportContent      = 'width=device-width, initial-scale=1';
        
    static $metaTitleXPath                  = 'head > meta[name="title"]';
    static $desiredMetaTitleContent         = 'Provide Text';

    static $titleXPath                      = 'head > title';
    static $desiredTitleContent             = 'WordPress Survey Plugin - SurveyFunnel';

    //static $metaDescriptionXPath            = '//*[@id="ls-global"]/head/meta[3]';
    static $metaDescriptionXPath            = 'head > meta[name="description"]';
    static $desiredMetaDescriptionContent   = 'WordPress Survey Plugin that boosts your opt-in and conversion rates using SurveyFunnel Techniques.';

    static $metaRobotsXPath                 = 'head > meta[name="robots"]';
    #static $desiredMetaRobotsContent        = 'max-snippet:-1, max-image-preview:large, max-video-preview:-1';
    static $desiredMetaRobotsContent        = 'index, follow';

    static $linkCanonicalXPath              = 'head > link[rel="canonical"]';
    static $desiredLinkCanonicalHref        = 'https://www.surveyfunnel.com/';

    static $metaOgTitleXPath                = 'head > meta[property="og:title"]';
    static $desiredMetaOgTitleContent       = 'WordPress Survey Plugin - SurveyFunnel';

    static $metaOgDescriptionXPath          = 'head > meta[property="og:description"]';
    static $desiredMetaOgDescriptionContent = 'WordPress Survey Plugin that boosts your opt-in and conversion rates using SurveyFunnel Techniques.';
}

 ?>
