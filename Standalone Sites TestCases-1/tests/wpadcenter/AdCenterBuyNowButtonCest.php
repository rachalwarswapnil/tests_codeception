<?php
require_once '_bootstrap.php';
class AdCenterBuyNowButtonCest
{

    public function _after(AcceptanceTester $I){
    	$I->amOnUrl("https://club.wpeka.com/cart/");
        $I->waitForElementVisible(_CheckoutPage::$RemoveButton,15);
        $I->click(_CheckoutPage::$RemoveButton);
        $I->waitForText('Your cart is currently empty.',20);
        $I->see('Your cart is currently empty.');
	}

    public function AdCenterBuyNowButton(AcceptanceTester $I){
    	$email=Data::uniqueEmail();
		$I->amOnPage('/');
		$I->scrollTo(_CheckoutPage::$homePageBuyNowButton);
		$I->click(_CheckoutPage::$homePageBuyNowButton);

		$I->wait(2);
		$I->scrollTo(_CheckoutPage::$BuynowButton);
		$I->click(_CheckoutPage::$BuynowButton);
		
		$I->wait(20);
		$I->scrollTo(_CheckoutPage::$Stripe_CreditcardButton);
		$I->waitForElementVisible(_CheckoutPage::$Stripe_CreditcardButton,30);
		$I->click(_CheckoutPage::$Stripe_CreditcardButton);
		$I->fillField(_CheckoutPage::$billingEmail_fieldSelector,$email);
		$I->wait(3);
		$Val = $I->grabAttributeFrom(_CheckoutPage::$CarNuberiframeSelector,'name');
		$I->switchToIFrame($Val);
		$I->fillField(_CheckoutPage::$CardNumberField, "4111111111111111");
		$I->switchToIFrame();
		$Val1 = $I->grabAttributeFrom(_CheckoutPage::$ExpDateiframeSelector,'name');
		$I->switchToIFrame($Val1);
		$I->fillField(_CheckoutPage::$ExpDateField, '1222');
		$I->switchToIFrame();
		$Val2 = $I->grabAttributeFrom(_CheckoutPage::$CvvNumberiframeSelector,'name');
		$I->switchToIFrame($Val2);
		$I->fillField(_CheckoutPage::$CVVNumberField, '122');
		$I->switchToIFrame();
		$I->click('#place_order');
		$I->wait(5);
		$I->waitForText('Unable to process this payment, please try again or use alternative method.',20);
		$I->see('Unable to process this payment, please try again or use alternative method.');

	}
}
?>