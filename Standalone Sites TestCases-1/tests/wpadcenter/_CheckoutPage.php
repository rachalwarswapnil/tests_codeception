<?php
class _CheckoutPage
{
    static $PopupSelector='#om-pxoik9nvnicf0x0yiduk-optin > div > button > svg > path';
    static $billingEmail_fieldSelector='#billing_email';
    static $BuynowButton="body > div.elementor.elementor-46409.elementor-location-single.post-74032.product.type-product.status-publish.has-post-thumbnail.product_cat-club.product_cat-elementor-template.product_cat-limited-sites-license.product_cat-plugins.product_tag-ad.product_tag-ads.product_tag-advertise.product_tag-advertisement.product_tag-banners.product_tag-campaigns.product_tag-ecommerce.product_tag-manager.product_tag-paypal.product_tag-sales.product_tag-statistics.member-discount.discount-restricted.first.instock.featured.sold-individually.shipping-taxable.purchasable.product-type-variable-subscription.has-default-attributes.product > div > div > section.elementor-element.elementor-element-52755b2.elementor-section-stretched.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div.elementor-element.elementor-element-6a9976d.elementor-column.elementor-col-33.elementor-top-column > div > div > section.elementor-element.elementor-element-2faea08.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-inner-section > div > div > div > div > div > div.elementor-element.elementor-element-8f51321.elementor-widget.elementor-widget-woocommerce-product-add-to-cart > div > div > form > div > div.woocommerce-variation-add-to-cart.variations_button.woocommerce-variation-add-to-cart-enabled > button";
    static $Stripe_CreditcardButton='#payment_method_stripe';
    static $checkout_PlaceOrderButton="button[name='woocommerce_checkout_place_order']";
    static $Confirm_PayText='Confirm and Pay';
    static $Place_OrderButton='#stripe-payment-data > button';
    static $IframeSelector='stripe_checkout_app';
    // static $credit_cardFieldSelector="input[placeholder='Card number']";
    // static $Exp_DateField="input[placeholder='MM / YY']";
    // static $CVVField="input[placeholder='CVC']";
    static $PayNowButton='#container > section > span:nth-child(3) > div > div > main > form > nav > div > div > div > button';

     Static $Wp_answer_BuyNow='.buy-now-bttuon.elementor-widget.elementor-widget-shortcode > div > div > div > a';

     static $buyNow1="//span[contains(text(),'BUY NOW')]";
    static $HeaderBuyNow="//a[contains(text(),'Buy Now')]";

    static $BuyNowLegalPageButton="//span[contains(text(),'Buy WPLegalPages Now')]";
    static $WPgdprButton="//span[contains(text(),'Learn More About WP GDPR Cookie Consent')]";
    static $CardNumberField="input[name='cardnumber']";
    static $ExpDateField="input[name='exp-date']";
    static $CVVNumberField="input[name='cvc']";


    static $CarNuberiframeSelector='#stripe-card-element > div > iframe';
    static $ExpDateiframeSelector='#stripe-exp-element > div > iframe';
    static $CvvNumberiframeSelector='#stripe-cvc-element > div > iframe';

    static $RemoveButton="//a[@class='remove']";


    static $MenuBuyNowButton                = '#menu-item-1510';
    static $homePageBuyNowButton            = '#pricing > div > div > div.elementor-element.elementor-element-6e406d65.elementor-column.elementor-col-33.elementor-top-column > div > div > div > div > div.elementor-price-table > div.elementor-price-table__footer > a';


    
}

 ?>
