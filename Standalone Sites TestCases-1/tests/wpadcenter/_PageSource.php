<?php
class _PageSource
{
    static $metaViewportXPath               = 'head > meta[name="viewport"]';
    static $desiredMetaViewportContent      = 'width=device-width, initial-scale=1';
        
    static $metaTitleXPath                  = 'head > meta[name="title"]';
    static $desiredMetaTitleContent         = 'Provide Text';
    static $titleXPath                      = 'head > title';
    static $desiredTitleContent             = 'WP AdCenter - WordPress Advertising Management Plugin';

    //static $metaDescriptionXPath            = '//*[@id="ls-global"]/head/meta[3]';
    static $metaDescriptionXPath            = 'head > meta[name="description"]';
    static $desiredMetaDescriptionContent   = 'WP Ad Center controls every aspect of the advertising on any WordPress powered website & provides detailed statistics to understand and tweak your ads.';

    static $metaRobotsXPath                 = 'head > meta[name="robots"]';
    #static $desiredMetaRobotsContent        = 'max-snippet:-1, max-image-preview:large, max-video-preview:-1';
    static $desiredMetaRobotsContent        = 'index, follow';

    static $linkCanonicalXPath              = 'head > link[rel="canonical"]';
    static $desiredLinkCanonicalHref        = 'https://wpadcenter.com/';

    static $metaOgTitleXPath                = 'head > meta[property="og:title"]';
    static $desiredMetaOgTitleContent       = 'WP AdCenter - WordPress Advertising Management Plugin';

    static $metaOgDescriptionXPath          = 'head > meta[property="og:description"]';
    static $desiredMetaOgDescriptionContent = 'WP Ad Center controls every aspect of the advertising on any WordPress powered website & provides detailed statistics to understand and tweak your ads.';
}

 ?>
