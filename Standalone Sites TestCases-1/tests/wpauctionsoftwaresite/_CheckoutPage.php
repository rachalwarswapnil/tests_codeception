<?php
class _CheckoutPage
{
    static $PopupSelector='#om-pxoik9nvnicf0x0yiduk-optin > div > button > svg > path';
    static $billingEmail_fieldSelector='#billing_email';
    static $BuynowButton="button[name='add-to-cart']";
    static $Stripe_Selector='#payment_method_stripe';
    static $checkout_PlaceOrderButton="button[name='woocommerce_checkout_place_order']";
    static $Confirm_PayText='Confirm and Pay';
    static $Place_OrderButton='#stripe-payment-data > button';
    static $IframeSelector='stripe_checkout_app';
    static $PayNowButton='#container > section > span:nth-child(3) > div > div > main > form > nav > div > div > div > button';

 Static $Wp_answer_BuyNow='.buy-now-bttuon.elementor-widget.elementor-widget-shortcode > div > div > div > a';

static $AuctionBuyNoHeaderButton=".menu-item-top-level.menu-item-top-level-6:nth-child(6) a:nth-child(1) > span.avia-menu-text";
//static $AuctionBuyNowButton1="#layerslider_1 > div.ls-inner > div > div:nth-child(6) > a";
static $AuctionBuyNowButton1="#layerslider_1 > div.ls-inner > div > div:nth-child(6) > a";
static $AuctionBuyNowButton2="//div[@id='av_section_2']//a[contains(@class,'avia-icon_select-no avia-color-custom avia-size-x-large avia-position-center')]";
static $CardNumberField="input[name='cardnumber']";
static $ExpDateField="input[name='exp-date']";
static $CVVNumberField="input[name='cvc']";


static $CarNuberiframeSelector='#stripe-card-element > div > iframe';
static $ExpDateiframeSelector='#stripe-exp-element > div > iframe';
static $CvvNumberiframeSelector='#stripe-cvc-element > div > iframe';
static $RemoveButton="//a[@class='remove']";

	static $addToCartButton 				= '.single_add_to_cart_button';
	static $BuyOnWPekaButton1 				= 'body > div.site.hfeed > div > div > div > section.elementor-element.elementor-element-ab4b7a9.elementor-section-content-middle.elementor-section-stretched.elementor-reverse-tablet.elementor-reverse-mobile.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div.elementor-container.elementor-column-gap-default > div > div.elementor-element.elementor-element-14531b6d.elementor-column.elementor-col-50.elementor-top-column > div > div > div.elementor-element.elementor-element-74ce4fbc.elementor-align-left.elementor-tablet-align-center.elementor-mobile-align-center.elementor-widget.elementor-widget-button > div > div > a';
	static $BuyOnWPekaButton2 				= 'body > div.site.hfeed > div > div > div > section.elementor-element.elementor-element-ad67ed8.elementor-section-height-min-height.elementor-section-content-middle.elementor-section-stretched.elementor-section-boxed.elementor-section-height-default.elementor-section-items-middle.elementor-section.elementor-top-section > div.elementor-container.elementor-column-gap-default > div > div > div > div > div.elementor-element.elementor-element-09b6514.elementor-align-center.elementor-widget.elementor-widget-button > div > div > a';


}

 ?>
