<?php
require_once '_bootstrap.php';
class BuyOnWPekaButton1TestCest
{

    public function _after(AcceptanceTester $I){
    	$I->amOnUrl("https://club.wpeka.com/cart/");
        $I->waitForElementVisible(_CheckoutPage::$RemoveButton,40);
        $I->click(_CheckoutPage::$RemoveButton);
        $I->waitForText('Your cart is currently empty.',60);
        $I->see('Your cart is currently empty.');
    }

    public function BuyOnWPekaButton1Cest(AcceptanceTester $I){
    	$email=Data::uniqueEmail();
		$I->amOnPage('/');
		$I->waitForElementVisible(_CheckoutPage::$BuyOnWPekaButton1,20);
		$I->click(_CheckoutPage::$BuyOnWPekaButton1);
		$I->wait(3);
		
		$I->see('Woo Auction Software');
        $I->scrollTo(_CheckoutPage::$addToCartButton);
        $I->click(_CheckoutPage::$addToCartButton);
        $I->wait(3);
        $I->click(_CheckoutPage::$Stripe_Selector);
        $I->fillField(_CheckoutPage::$billingEmail_fieldSelector,$email);
        $I->wait(3);
        $Val = $I->grabAttributeFrom(_CheckoutPage::$CarNuberiframeSelector,'name');
        $I->switchToIFrame($Val);
        $I->fillField(_CheckoutPage::$CardNumberField, "4111111111111111");
        $I->switchToIFrame();
        $Val1 = $I->grabAttributeFrom(_CheckoutPage::$ExpDateiframeSelector,'name');
        $I->switchToIFrame($Val1);
        $I->fillField(_CheckoutPage::$ExpDateField, '1222');
        $I->switchToIFrame();
        $Val2 = $I->grabAttributeFrom(_CheckoutPage::$CvvNumberiframeSelector,'name');
        $I->switchToIFrame($Val2);
        $I->fillField(_CheckoutPage::$CVVNumberField, '122');
        $I->switchToIFrame();
        $I->click('#place_order');
        $I->wait(5);
        $I->waitForText('Unable to process this payment, please try again or use alternative method.',20);
        $I->see('Unable to process this payment, please try again or use alternative method.');
    }
}
?>