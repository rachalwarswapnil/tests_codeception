<?php
class _PageSource
{
    static $metaViewportXPath               = 'head > meta[name="viewport"]';
    static $desiredMetaViewportContent      = 'width=device-width, initial-scale=1';
        
    static $metaTitleXPath                  = 'head > meta[name="title"]';
    static $desiredMetaTitleContent         = 'WP Auction Software - Penny, Reverse, Simple Auctions using WooCommerce';

    static $titleXPath                      = 'head > title';
    #static $desiredTitleContent             = 'Responsive WordPress Auction Software | WordPress Auction Software';
    static $desiredTitleContent             = 'WP Auction Software plugin allows easy and quick way to setup auctions on your site or Multi-Vendor websites. Penny Auction, Reverse, Simple. Feature rich & Easy.';


    //static $metaDescriptionXPath            = '//*[@id="ls-global"]/head/meta[3]';
    static $metaDescriptionXPath            = 'head > meta[name="description"]';
    static $desiredMetaDescriptionContent   = 'WP Auction Software plugin allows easy and quick way to setup auctions on your site or Multi-Vendor websites. Penny Auction, Reverse, Simple. Feature rich & Easy.';

    static $metaRobotsXPath                 = 'head > meta[name="robots"]';
    #static $desiredMetaRobotsContent        = 'max-snippet:-1, max-image-preview:large, max-video-preview:-1';
    static $desiredMetaRobotsContent        = 'index, follow';

    static $linkCanonicalXPath              = 'head > link[rel="canonical"]';
    static $desiredLinkCanonicalHref        = 'https://www.wpauctionsoftware.com/';

    static $metaOgTitleXPath                = 'head > meta[property="og:title"]';
    static $desiredMetaOgTitleContent       = 'WP Auction Software - Penny, Reverse, Simple Auctions using WooCommerce';

    static $metaOgDescriptionXPath          = 'head > meta[property="og:description"]';
    static $desiredMetaOgDescriptionContent = 'WP Auction Software plugin allows easy and quick way to setup auctions on your site or Multi-Vendor websites. Penny Auction, Reverse, Simple. Feature rich & Easy.';
}

 ?>
