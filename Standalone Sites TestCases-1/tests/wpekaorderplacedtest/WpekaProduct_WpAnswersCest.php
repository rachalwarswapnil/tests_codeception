<?php

require_once '_bootstrap.php';
class WpekaProduct_WpAnswersCest
{

    public function _after(AcceptanceTester $I)

    {
        $I->amOnPage("/cart");
        $I->waitForElementVisible(_CheckoutPage::$RemoveButton,40);
        $I->click(_CheckoutPage::$RemoveButton);
        $I->waitForText('Your cart is currently empty.',60);
        $I->see('Your cart is currently empty.');

    }

    public function WpekaProductWpAnswers(AcceptanceTester $I)
{
$email=Data::uniqueEmail();
$I->amOnPage('/product/wp-answers/');
$I->waitForElementVisible(_CheckoutPage::$BuynowButton,20);
$I->dontSee('[ppmtoggle');

$I->click(_CheckoutPage::$BuynowButton);
// $I->waitForElementVisible('body > main > div > div.woocommerce > div.cart-collaterals > div > div > a',30);
// $I->scrollTo('body > main > div > div.woocommerce > div.cart-collaterals > div > div > a',30);
//
// $I->click('body > main > div > div.woocommerce > div.cart-collaterals > div > div > a');


$I->wait(20);
$I->waitForElementVisible(_CheckoutPage::$Stripe_CreditcardButton,30);
$I->scrollTo(_CheckoutPage::$Stripe_CreditcardButton);

$I->click(_CheckoutPage::$Stripe_CreditcardButton);
$I->fillField(_CheckoutPage::$billingEmail_fieldSelector,$email);

$Val = $I->grabAttributeFrom(_CheckoutPage::$CarNuberiframeSelector,'name');
$I->switchToIFrame($Val);
$I->fillField(_CheckoutPage::$CardNumberField, "4111111111111111");
$I->switchToIFrame();
$Val1 = $I->grabAttributeFrom(_CheckoutPage::$ExpDateiframeSelector,'name');
$I->switchToIFrame($Val1);
$I->fillField(_CheckoutPage::$ExpDateField, '1222');
$I->switchToIFrame();
$Val2 = $I->grabAttributeFrom(_CheckoutPage::$CvvNumberiframeSelector,'name');
$I->switchToIFrame($Val2);
$I->fillField(_CheckoutPage::$CVVNumberField, '122');
$I->switchToIFrame();
$I->click('#place_order');
$I->wait(5);
$I->waitForText('Unable to process this payment, please try again or use alternative method.',20);

$I->see('Unable to process this payment, please try again or use alternative method.');
}
}

?>
