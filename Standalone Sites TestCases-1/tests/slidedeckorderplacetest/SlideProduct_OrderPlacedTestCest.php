<?php
require_once '_bootstrap.php';


class SlideProduct_OrderPlacedTestCest{
    public function _after(AcceptanceTester $I){
        $I->amOnPage("/cart");
        $I->waitForElementVisible(_SlidedeckCheckoutPage::$RemoveButton,40);
        $I->click(_SlidedeckCheckoutPage::$RemoveButton);
        $I->waitForText('Your cart is currently empty.',60);
        $I->see('Your cart is currently empty.');
    }

    public function slideproductorderplace(AcceptanceTester $I){
        $email=Data::uniqueEmail();
        $I->amOnPage('/slidedeck5-pricing/');
        $I->waitForElementVisible(_SlidedeckCheckoutPage::$BuynowButton,60);
        $I->scrollTo(_SlidedeckCheckoutPage::$BuynowButton);
        $I->click(_SlidedeckCheckoutPage::$BuynowButton);
        $I->wait(10);
        $I->waitForElementVisible(_SlidedeckCheckoutPage::$ProceedTocheckoutButton,30);
        $I->scrollTo(_SlidedeckCheckoutPage::$ProceedTocheckoutButton);
        $I->click(_SlidedeckCheckoutPage::$ProceedTocheckoutButton);
        $I->waitForElementVisible(_SlidedeckCheckoutPage::$billingEmail_fieldSelector,20);
        $I->fillField(_SlidedeckCheckoutPage::$billingEmail_fieldSelector,$email);
        $I->fillField(_SlidedeckCheckoutPage::$PasswordField,'rupesh1395');
        $Val = $I->grabAttributeFrom(_SlidedeckCheckoutPage::$CarNuberiframeSelector,'name');
        $I->switchToIFrame($Val);
        $I->scrollTo(_SlidedeckCheckoutPage::$CardNumberField);
        $I->fillField(_SlidedeckCheckoutPage::$CardNumberField, "4111111111111111");
        $I->switchToIFrame();
        $Val1 = $I->grabAttributeFrom(_SlidedeckCheckoutPage::$ExpDateiframeSelector,'name');
        $I->switchToIFrame($Val1);
        $I->fillField(_SlidedeckCheckoutPage::$ExpDateField, '1222');
        $I->switchToIFrame();
        $Val2 = $I->grabAttributeFrom(_SlidedeckCheckoutPage::$CvvNumberiframeSelector,'name');
        $I->switchToIFrame($Val2);
        $I->fillField(_SlidedeckCheckoutPage::$CVVNumberField, '122');
        $I->switchToIFrame();
        $I->scrollTo(_SlidedeckCheckoutPage::$placeOrderButton);
        $I->click('#place_order');
        $I->wait(5);
        $I->waitForText('Unable to process this payment, please try again or use alternative method.',20);
        $I->see('Unable to process this payment, please try again or use alternative method.');
    }
}
?>
