<?php
class _SlidedeckCheckoutPage
{

    static $PopupSelector='#om-pxoik9nvnicf0x0yiduk-optin > div > button > svg > path';
    static $billingEmail_fieldSelector='#billing_email';
    static $BuynowButton="//div[@class='elementor-element elementor-element-258497c elementor-widget elementor-widget-price-table']//a[@class='elementor-price-table__button elementor-button elementor-size-lg'][contains(text(),'BUY NOW')]";
    static $ProceedTocheckoutButton='div > div > div.cart-collaterals > div > div > a';
    static $PasswordField='#account_password';
    static $CarNuberiframeSelector='#stripe-card-element > div > iframe';
    static $ExpDateiframeSelector='#stripe-exp-element > div > iframe';
    static $CvvNumberiframeSelector='#stripe-cvc-element > div > iframe';

    static $CardNumberField="input[name='cardnumber']";
    static $ExpDateField="input[name='exp-date']";
    static $CVVNumberField="input[name='cvc']";
    static $placeOrderButton='#place_order';
    static $Place_OrderButton='#stripe-payment-data > button';
    static $IframeSelector='stripe_checkout_app';
    static $credit_cardFieldSelector="input[placeholder='Card number']";
    static $Exp_DateField="input[placeholder='MM / YY']";
    static $CVVField="input[placeholder='CVC']";
    static $PayNowButton='#container > section > span:nth-child(3) > div > div > main > form > nav > div > div > div > button';

    static $RemoveButton="//a[@class='remove']";







}

 ?>
