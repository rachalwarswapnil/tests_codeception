<?php
class _PageSource
{
    static $metaViewportXPath               = 'head > meta[name="viewport"]';
    //static $metaViewportXPath               = '/html/head/meta[2]';
    static $desiredMetaViewportContent      = 'width=device-width, initial-scale=1';
        
    static $metaTitleXPath                  = 'head > meta[name="title"]';
    static $desiredMetaTitleContent         = 'Provide Text';
    static $titleXPath                      = 'head > title';
    static $desiredTitleContent             = 'Responsive WordPress Slider Plugin - SlideDeck';

    //static $metaDescriptionXPath            = '//*[@id="ls-global"]/head/meta[3]';
    static $metaDescriptionXPath            = 'head > meta[name="description"]';
    static $desiredMetaDescriptionContent   = 'Create a WordPress slider with almost anything - posts, pages, custom post types, WooCommerce products, images, and videos. Get SlideDeck for free.';

    static $metaRobotsXPath                 = 'head > meta[name="robots"]';
    #static $desiredMetaRobotsContent        = 'max-snippet:-1, max-image-preview:large, max-video-preview:-1';
    static $desiredMetaRobotsContent        = 'index, follow';
    
    static $linkCanonicalXPath              = 'head > link[rel="canonical"]';
    static $desiredLinkCanonicalHref        = 'https://www.slidedeck.com/';

    static $metaOgTitleXPath                = 'head > meta[property="og:title"]';
    static $desiredMetaOgTitleContent       = 'Responsive WordPress Slider Plugin - SlideDeck';

    static $metaOgDescriptionXPath          = 'head > meta[property="og:description"]';
    static $desiredMetaOgDescriptionContent = 'Create a WordPress slider with almost anything - posts, pages, custom post types, WooCommerce products, images, and videos. Get SlideDeck for free.';
}

 ?>
