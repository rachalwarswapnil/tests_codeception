<?php
class PromotePage{
	static $PromoteMenuLink = '#menu-item-36792';
	static $PopUp = '#om-m98vbwwzjiu7qccw68mb-optin > div > button > svg';
    
    static $BuyLink300Scroll='#post-27086 > div.entry.entry-content > h2:nth-child(4)';
    static $BuyLink300 = '#edd_purchase_36732 > div > a.edd-add-to-cart.button.blue.edd-submit.edd-has-js';
    
    static $BuyLink450Scroll='#post-27086 > div.entry.entry-content > div.wp-block-group.wpeka-promotion-options > div > div:nth-child(1) > div > h4';
    static $BuyLink450='#edd_purchase_36735 > div > a.edd-add-to-cart.button.blue.edd-submit.edd-has-js';
    
    static $BuyLink50Scroll='#post-27086 > div.entry.entry-content > div.wp-block-group.wpeka-promotion-options > div > div:nth-child(2) > div > h4';
    static $BuyLink50='#edd_purchase_36719 > div > a.edd-add-to-cart.button.blue.edd-submit.edd-has-js';
}

 ?>
