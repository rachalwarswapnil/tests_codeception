<?php
require_once '_bootstrap.php';
class WpekaBlogOrder300Cest
{
	public function WpekaPromotePageOrder1(AcceptanceTester $I){
    	$email=Data::uniqueEmail();
		$I->amOnPage('/');
		$I->wait(3);
		$I->click('//*[@id="cookie_action_accept"]');
        $I->wait(3);
        $I->see('PROMOTE');
		
		$I->wait(2);
		//$I->waitForElementVisible(PromotePage::$PopUp,20);
		//$I->click(PromotePage::$PopUp);

		$I->click(PromotePage::$PromoteMenuLink);
		$I->wait(2);
		//$I->scrollTo(PromotePage::$BuyLink300Scroll);
		$I->wait(5);
		//text above btn
		//$I->scrollTo('//*[@id="post-27086"]/div[3]/div[1]/div/div[1]/div/h4');
		//btn
		$I->wait(500);
		//$I->click('//*[@id="edd_purchase_36732"]/div/a[1]/span[1]');

        $I->click('$300.00 – Purchase');
		$I->wait(2);
        //$I->click('//*[@id="edd_purchase_36732"]/div/a[1]/span[1]');

        //$I->click(PromotePage::$BuyLink300);
		$I->wait(20);
		$I->fillField(CheckoutPage::$email,$email);
		$I->fillField(CheckoutPage::$first,'FirstName');
		$I->fillField(CheckoutPage::$last,'LastName');
		$I->click(CheckoutPage::$purchase);
		$I->wait(7);
		//$uri = $I->grabFromCurrentUrl();
		$uri = $I->executeJS("return location.href");
		$I->assertContains('https://www.paypal.com/webapps',$uri);
	}
}
?>
