<?php
class _PageSource
{
    static $metaViewportXPath               = 'head > meta[name="viewport"]';
    static $desiredMetaViewportContent      = 'width=device-width, initial-scale=1';
        
    static $metaTitleXPath                  = 'head > meta[name="title"]';
    static $desiredMetaTitleContent         = 'Provide Text';
    static $titleXPath                      = 'head > title';
    static $desiredTitleContent             = 'Local Business Directory Builder Plugin for WordPress - WPLocalPlus';

    //static $metaDescriptionXPath            = '//*[@id="ls-global"]/head/meta[3]';
    static $metaDescriptionXPath            = 'head > meta[name="description"]';
    static $desiredMetaDescriptionContent   = 'WP Local Plus WordPress plugin helps you automatically create local business directories complete with reviews, coupons & Google Maps integration...';

    static $metaRobotsXPath                 = 'head > meta[name="robots"]';
    #static $desiredMetaRobotsContent        = 'max-snippet:-1, max-image-preview:large, max-video-preview:-1';
    static $desiredMetaRobotsContent        = 'index, follow';

    static $linkCanonicalXPath              = 'head > link[rel="canonical"]';
    static $desiredLinkCanonicalHref        = 'https://www.wplocalplus.com/';

    static $metaOgTitleXPath                = 'head > meta[property="og:title"]';
    static $desiredMetaOgTitleContent       = 'Local Business Directory Builder Plugin for WordPress - WPLocalPlus';

    static $metaOgDescriptionXPath          = 'head > meta[property="og:description"]';
    static $desiredMetaOgDescriptionContent = 'WP Local Plus WordPress plugin helps you automatically create local business directories complete with reviews, coupons & Google Maps integration...';
}

 ?>
