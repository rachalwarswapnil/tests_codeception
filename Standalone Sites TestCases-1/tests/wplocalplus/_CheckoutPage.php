<?php
class _CheckoutPage
{
    static $PopupSelector='#om-pxoik9nvnicf0x0yiduk-optin > div > button > svg > path';
    static $billingEmail_fieldSelector='#billing_email';
    static $BuynowButton="button[name='add-to-cart']";
    static $Stripe_CreditcardButton='#payment_method_stripe';
    static $checkout_PlaceOrderButton="button[name='woocommerce_checkout_place_order']";
    static $Confirm_PayText='Confirm and Pay';
    static $Place_OrderButton='#stripe-payment-data > button';
    static $IframeSelector='stripe_checkout_app';
    // static $credit_cardFieldSelector="input[placeholder='Card number']";
    // static $Exp_DateField="input[placeholder='MM / YY']";
    // static $CVVField="input[placeholder='CVC']";
    static $PayNowButton='#container > section > span:nth-child(3) > div > div > main > form > nav > div > div > div > button';

     Static $Wp_answer_BuyNow='.buy-now-bttuon.elementor-widget.elementor-widget-shortcode > div > div > div > a';

     static $buyNow1="//span[contains(text(),'BUY NOW')]";
    static $HeaderBuyNow="//a[contains(text(),'Buy Now')]";

    static $BuyNowLegalPageButton="//span[contains(text(),'Buy WPLegalPages Now')]";
    static $WPgdprButton="//span[contains(text(),'Learn More About WP GDPR Cookie Consent')]";
    static $CardNumberField="input[name='cardnumber']";
    static $ExpDateField="input[name='exp-date']";
    static $CVVNumberField="input[name='cvc']";


    static $CarNuberiframeSelector='#stripe-card-element > div > iframe';
    static $ExpDateiframeSelector='#stripe-exp-element > div > iframe';
    static $CvvNumberiframeSelector='#stripe-cvc-element > div > iframe';

    static $RemoveButton="//a[@class='remove']";


    #static $homePageBeforeBuyNowButton          = '#x-section-6 > div > div > h2 > span';
    static $homePageBeforeBuyNowButton          = 'body > div.site.hfeed > div > div > div > section.elementor-element.elementor-element-e0a44dd.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div > div > div > div.elementor-element.elementor-element-76c1d2e.elementor-widget.elementor-widget-heading > div';
    #static $homePageBuyNowButton                = "//a[contains(text(),'BUY NOW')]";
    static $homePageBuyNowButton                = "body > div.site.hfeed > div > div > div > section.elementor-element.elementor-element-e0a44dd.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div > div > div > div.elementor-element.elementor-element-26dc596.elementor-align-center.elementor-widget.elementor-widget-button > div > div > a";
    

    #static $pricingPageBeforeBuyNowButton       = '#x-section-1 > div:nth-child(2) > div > div > div > div > h3';
    #static $pricingPageBuyNowButton             = '#x-section-1 > div:nth-child(2) > div > div > div > div > a';
    static $pricingPageBuyNowButton             = '#post-4144 > div > div.entry-content > div > div > div > section > div > div > div > div > div > div.elementor-element.elementor-element-6bf2cf0.elementor-widget.elementor-widget-price-table > div > div.elementor-price-table > div.elementor-price-table__footer > a';
    


}

 ?>
