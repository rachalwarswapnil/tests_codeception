<?php
class _PageSource
{
    static $metaViewportXPath               = 'head > meta[name="viewport"]';
    static $desiredMetaViewportContent      = 'width=device-width, initial-scale=1';
        
    static $metaTitleXPath                  = 'head > meta[name="title"]';
    static $desiredMetaTitleContent         = 'PROVIDE TEXT';
    
    static $titleXPath                      = 'head > title';
    static $desiredTitleContent             = 'WordPress Privacy Policy Plugin (GDPR, CCPA) – WP Legal Pages';

    static $metaDescriptionXPath            = 'head > meta[name="description"]';
    //static $desiredMetaDescriptionContent   = 'A Plugin that helps you be compliant with Privacy and Legal requirements. Terms of Service, Legal Policies - Create Legal Pages for all your Website needs.';
    static $desiredMetaDescriptionContent   = 'WordPress privacy policy generator plugin for GDPR & CCPA. Generates 25+ policies including Terms & Conditions, and Affiliate policies. Get it for free.'; //Updated on 27 January

    static $metaRobotsXPath                 = 'head > meta[name="robots"]';
    #static $desiredMetaRobotsContent        = 'max-snippet:-1, max-image-preview:large, max-video-preview:-1';
    static $desiredMetaRobotsContent        = 'index, follow';

    static $linkCanonicalXPath              = 'head > link[rel="canonical"]';
    static $desiredLinkCanonicalHref        = 'https://wplegalpages.com/';

    static $metaOgTitleXPath                = 'head > meta[property="og:title"]';
    //static $desiredMetaOgTitleContent       = 'WordPress Terms of Service & Privacy Policy Plugin - WP Legal Pages (GDPR, CCPA Compliant)';
    static $desiredMetaOgTitleContent       = 'WordPress Privacy Policy Plugin (GDPR, CCPA) – WP Legal Pages'; //Updated on 27 January

    static $metaOgDescriptionXPath          = 'head > meta[property="og:description"]';
    //static $desiredMetaOgDescriptionContent = 'A Plugin that helps you be compliant with Privacy and Legal requirements. Terms of Service, Legal Policies - Create Legal Pages for all your Website needs.';
    static $desiredMetaOgDescriptionContent = 'WordPress privacy policy generator plugin for GDPR & CCPA. Generates 25+ policies including Terms & Conditions, and Affiliate policies. Get it for free.'; //Updated on 27 January
    
}

 ?>
