<?php
require_once '_bootstrap.php';
class BuyNowButtonInMenuTestCest
{

    /*public function _after(AcceptanceTester $I){
        $I->amOnUrl("https://club.wpeka.com/cart/");
        $I->waitForElementVisible(_CheckoutPage::$RemoveButton,40);
        $I->click(_CheckoutPage::$RemoveButton);
        $I->waitForText('Your cart is currently empty.',60);
        $I->see('Your cart is currently empty.');
        
    }*/

    public function DownloadLinkInMenuForWordPressOrg(AcceptanceTester $I){
        $email=Data::uniqueEmail();
        $I->amOnPage('/');
        $I->waitForElement('//*[@id="menu-item-62"]/a');
        //$I->wait(5);
        $I->click('//*[@id="menu-item-62"]/a');
        $I->wait(5);
        $I->waitForElement('/html/body/div[3]/div/div/section[2]/div/div/div[2]/div/div/section[1]/div/div/div/div/div/div[7]/div/div/form/div/div[2]/button', 40);
        $I->scrollTo('body > div.elementor.elementor-46409.elementor-location-single.post-73708.product.type-product.status-publish.has-post-thumbnail.product_cat-club.product_cat-elementor-template.product_cat-homepage-grid.product_cat-limited-sites-license.product_cat-plugins.product_cat-woocommerce-plugins.product_tag-legal.product_tag-privacy.product_tag-terms-and-conditions.member-discount.discount-restricted.first.instock.featured.sold-individually.shipping-taxable.purchasable.product-type-variable-subscription.has-default-attributes.product > div > div > section.elementor-element.elementor-element-52755b2.elementor-section-stretched.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div.elementor-element.elementor-element-6a9976d.elementor-column.elementor-col-33.elementor-top-column > div > div > section.elementor-element.elementor-element-2faea08.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-inner-section > div > div > div > div > div > div.elementor-element.elementor-element-8f51321.elementor-widget.elementor-widget-woocommerce-product-add-to-cart > div > div > form > table > tbody > tr > td.label > label');
        $I->wait(2);
        $I->click('/html/body/div[3]/div/div/section[2]/div/div/div[2]/div/div/section[1]/div/div/div/div/div/div[7]/div/div/form/div/div[2]/button');
        $I->wait(2);
        $I->waitForElementVisible(_CheckoutPage::$Stripe_CreditcardButton,30);
        $I->see('WP Legal Pages Pro');

        $I->scrollTo(_CheckoutPage::$Stripe_CreditcardButton);
        $I->waitForElementVisible(_CheckoutPage::$Stripe_CreditcardButton,30);
        $I->click(_CheckoutPage::$Stripe_CreditcardButton);
        $I->fillField(_CheckoutPage::$billingEmail_fieldSelector,$email);
        $I->wait(3);
        $Val = $I->grabAttributeFrom(_CheckoutPage::$CarNuberiframeSelector,'name');
        $I->switchToIFrame($Val);
        $I->fillField(_CheckoutPage::$CardNumberField, "4111111111111111");
        $I->switchToIFrame();
        $Val1 = $I->grabAttributeFrom(_CheckoutPage::$ExpDateiframeSelector,'name');
        $I->switchToIFrame($Val1);
        $I->fillField(_CheckoutPage::$ExpDateField, '1222');
        $I->switchToIFrame();
        $Val2 = $I->grabAttributeFrom(_CheckoutPage::$CvvNumberiframeSelector,'name');
        $I->switchToIFrame($Val2);
        $I->fillField(_CheckoutPage::$CVVNumberField, '122');
        $I->switchToIFrame();
        $I->click('#place_order');
        $I->wait(5);
        $I->waitForText('Unable to process this payment, please try again or use alternative method.',20);
        $I->see('Unable to process this payment, please try again or use alternative method.');
        //$I->see('WordPress.org');
        //$I->see('Privacy Policy Generator WordPress Plugin: WP Legal Pages');
    }
}
?>
