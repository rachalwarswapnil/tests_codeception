<?php
require_once '_bootstrap.php';
class SeePageElementsTestCest
{
    public function checkCookieBarPoweredByLink(AcceptanceTester $I){
        $I->amOnPage('/');
        $I->waitForElement('#cookie_action_settings');
        $I->seeInSource('GDPR Cookie Consent Plugin</a></div>');
    }
}
?>