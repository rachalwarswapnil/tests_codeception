<?php

require_once '_bootstrap.php';
$I = new AcceptanceTester($scenario);

$email=Data::uniqueEmail();
$I->amOnPage('/');
$I->waitForElementVisible(_CheckoutPage::$WPgdprButton,20);
$I->scrollTo(_CheckoutPage::$WPgdprButton);

$I->click(_CheckoutPage::$WPgdprButton);

$I->wait(10);
$I->switchToNextTab();
$I->waitForText('WordPress Cookie Consent Plugin for GDPR & CCPA',30);
$I->see('WordPress Cookie Consent Plugin for GDPR & CCPA');

?>
