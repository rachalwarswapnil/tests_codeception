<?php
require_once '_bootstrap.php';
class SeeMetaElementsTestCest
{

    public function SeeMetaViewport(AcceptanceTester $I){
		$I->amOnPage('/');
        //Meta Viewport
        $I->seeNumberOfElementsInDOM(_PageSource::$metaViewportXPath, 1);
        $availableMetaViewportContent = $I->executeInSelenium(function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver){
            return $webdriver->findElement(WebDriverBy::cssSelector(_PageSource::$metaViewportXPath))->getAttribute('content');
        });
        $I->assertContains(_PageSource::$desiredMetaViewportContent,$availableMetaViewportContent);
    }

    public function SeeMetaTitle(AcceptanceTester $I){
        $I->amOnPage('/');
        //Meta Title or Title
        try {
            $I->seeElement(_PageSource::$metaTitleXPath);
            $I->seeNumberOfElementsInDOM(_PageSource::$metaTitleXPath, 1);
            $availableMetaTitleContent = $I->executeInSelenium(function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver){
                return $webdriver->findElement(WebDriverBy::cssSelector(_PageSource::$metaTitleXPath))->getAttribute('content');
            });
            $I->assertEquals(_PageSource::$desiredMetaDescriptionContent,$availableMetaTitleContent);
        } catch (Exception $e) {
            $I->seeElementInDOM(_PageSource::$titleXPath);
            $I->seeInTitle(_PageSource::$desiredTitleContent);
        }
    }
    
    public function SeeMetaDescription(AcceptanceTester $I){
        $I->amOnPage('/');
        //Meta Descripiton
        $I->seeNumberOfElementsInDOM(_PageSource::$metaDescriptionXPath, 1);
        $availableMetaDescriptionContent = $I->executeInSelenium(function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver){
            return $webdriver->findElement(WebDriverBy::cssSelector(_PageSource::$metaDescriptionXPath))->getAttribute('content');
        });
        $DescripitonLength = strlen($availableMetaDescriptionContent);
        $I->assertEquals(_PageSource::$desiredMetaDescriptionContent,$availableMetaDescriptionContent);
    }

    public function SeeMetaRobots(AcceptanceTester $I){
        $I->amOnPage('/');
        //Meta Robots
        $I->seeNumberOfElementsInDOM(_PageSource::$metaRobotsXPath, 1);
        $availableMetaRobots = $I->executeInSelenium(function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver){
            return $webdriver->findElement(WebDriverBy::cssSelector(_PageSource::$metaRobotsXPath))->getAttribute('content');
        });
        $I->assertEquals(_PageSource::$desiredMetaRobotsContent,$availableMetaRobots);
    }

    public function SeeCanonicalLinkHref(AcceptanceTester $I){
        $I->amOnPage('/');        
        //Link Canonical Reference
        $I->seeNumberOfElementsInDOM(_PageSource::$linkCanonicalXPath, 1);
        $availableLinkCanonicalHref = $I->executeInSelenium(function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver){
            return $webdriver->findElement(WebDriverBy::cssSelector(_PageSource::$linkCanonicalXPath))->getAttribute('href');
        });
        $I->assertEquals(_PageSource::$desiredLinkCanonicalHref,$availableLinkCanonicalHref);
    }

    public function SeeMetaOgTitle(AcceptanceTester $I){
        $I->amOnPage('/');       
        //Meta og Title
        $I->seeNumberOfElementsInDOM(_PageSource::$metaOgTitleXPath, 1);
        $availableMetaOgTitleContent = $I->executeInSelenium(function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver){
            return $webdriver->findElement(WebDriverBy::cssSelector(_PageSource::$metaOgTitleXPath))->getAttribute('content');
        });
        $I->assertEquals(_PageSource::$desiredMetaOgTitleContent,$availableMetaOgTitleContent);
    }

    public function SeeMetaOgDescription(AcceptanceTester $I){
        $I->amOnPage('/');        
        //Meta og Descripiton
        $I->seeNumberOfElementsInDOM(_PageSource::$metaOgDescriptionXPath, 1);
        $availableMetaOgDescriptionContent = $I->executeInSelenium(function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver){
            return $webdriver->findElement(WebDriverBy::cssSelector(_PageSource::$metaOgDescriptionXPath))->getAttribute('content');
        });
        $I->assertEquals(_PageSource::$desiredMetaOgDescriptionContent,$availableMetaOgDescriptionContent);
    }
}
?>