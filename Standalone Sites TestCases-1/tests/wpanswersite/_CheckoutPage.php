<?php
class _CheckoutPage
{
    static $PopupSelector='#om-pxoik9nvnicf0x0yiduk-optin > div > button > svg > path';
    static $billingEmail_fieldSelector='#billing_email';
    static $BuynowButton="button[name='add-to-cart']";
    static $Stripe_CreditcardButton='#payment_method_stripe';
    static $checkout_PlaceOrderButton="button[name='woocommerce_checkout_place_order']";
    static $Confirm_PayText='Confirm and Pay';
    static $Place_OrderButton='#stripe-payment-data > button';
    static $IframeSelector='stripe_checkout_app';
    static $credit_cardFieldSelector="input[placeholder='Card number']";
    static $Exp_DateField="input[placeholder='MM / YY']";
    static $CVVField="input[placeholder='CVC']";
    static $PayNowButton='#container > section > span:nth-child(3) > div > div > main > form > nav > div > div > div > button';

 Static $Wp_answer_BuyNow='.buy-now-bttuon.elementor-widget.elementor-widget-shortcode > div > div > div > a';

static $HeaderBuyNow="//a[contains(text(),'Buy On WPeka')]";

static $PurchaseNowButton1="//div[@class='elementor-element elementor-element-cfc8aea elementor-align-center elementor-widget elementor-widget-button']//a[@class='elementor-button-link elementor-button elementor-size-lg']";
static $PurchaseNowButton2="//div[@class='elementor-element elementor-element-3a500f0 elementor-align-center elementor-widget elementor-widget-button']//span[@class='elementor-button-text'][contains(text(),'BUY ON WPEKA')]";
static $ScrollSelctor2='body > div.elementor.elementor-492 > div > div > section.elementor-element.elementor-element-4d4b7e9.elementor-section-stretched.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div > div > div > section.elementor-element.elementor-element-cc5c1ca.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-inner-section > div > div > div.elementor-element.elementor-element-eaf74a0.elementor-column.elementor-col-33.elementor-inner-column > div > div > div > div > div > div > h3';

static $ScrollSelector1='body > div.elementor.elementor-492 > div > div > section.elementor-element.elementor-element-a17e499.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div > div > div > section.elementor-element.elementor-element-15e4519.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-inner-section > div > div > div.elementor-element.elementor-element-068a69e.elementor-column.elementor-col-33.elementor-inner-column > div > div > div > div > div > div.elementor-icon-box-content > h3 > span';
static $CardNumberField="input[name='cardnumber']";
static $ExpDateField="input[name='exp-date']";
static $CVVNumberField="input[name='cvc']";


static $CarNuberiframeSelector='#stripe-card-element > div > iframe';
static $ExpDateiframeSelector='#stripe-exp-element > div > iframe';
static $CvvNumberiframeSelector='#stripe-cvc-element > div > iframe';
static $RemoveButton="//a[@class='remove']";


}

 ?>
