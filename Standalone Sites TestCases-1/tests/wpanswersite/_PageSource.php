<?php
class _PageSource
{
    static $metaViewportXPath               = 'head > meta[name="viewport"]';
    //static $metaViewportXPath               = '/html/head/meta[2]';
    static $desiredMetaViewportContent      = 'width=device-width, initial-scale=1';
        
    static $metaTitleXPath                  = 'head > meta[name="title"]';
    static $desiredMetaTitleContent         = 'Provide Text';

    static $titleXPath                      = 'head > title';
    static $desiredTitleContent             = 'WordPress Question and Answer Plugin - WP-Answers: WordPress Question & Answer Plugin / Theme';

    //static $metaDescriptionXPath            = '//*[@id="ls-global"]/head/meta[3]';
    static $metaDescriptionXPath            = 'head > meta[name="description"]';
    static $desiredMetaDescriptionContent   = 'Popular WordPress Question & Answer Plugin + Theme. Automatically pulls data from Stackoverflow, so you can launch your Q&A site with a bang!';

    static $metaRobotsXPath                 = 'head > meta[name="robots"]';
    #static $desiredMetaRobotsContent        = 'max-snippet:-1, max-image-preview:large, max-video-preview:-1';
    static $desiredMetaRobotsContent        = 'index, follow';

    static $linkCanonicalXPath              = 'head > link[rel="canonical"]';
    static $desiredLinkCanonicalHref        = 'https://wp-answers.com/';

    static $metaOgTitleXPath                = 'head > meta[property="og:title"]';
    static $desiredMetaOgTitleContent       = 'WordPress Question and Answer Plugin - WP-Answers: WordPress Question & Answer Plugin / Theme';

    static $metaOgDescriptionXPath          = 'head > meta[property="og:description"]';
    static $desiredMetaOgDescriptionContent = 'Popular WordPress Question & Answer Plugin + Theme. Automatically pulls data from Stackoverflow, so you can launch your Q&A site with a bang!';
}

 ?>
