<?php
require_once '_bootstrap.php';

class BuyOnWPekaButton1TestCest
{

    /*public function _after(AcceptanceTester $I){
        $I->amOnUrl("https://club.wpeka.com/cart/");
        $I->waitForElementVisible(_CheckoutPage::$RemoveButton,40);
        $I->click(_CheckoutPage::$RemoveButton);
        $I->waitForText('Your cart is currently empty.',60);
        $I->see('Your cart is currently empty.');
    }*/

    public function BuyOnWPekaFirstButton(AcceptanceTester $I){
    	$email=Data::uniqueEmail();
		$I->amOnPage('/');
		$I->wait(5);
		$I->scrollTo('body > div.elementor.elementor-492 > div > div > section.elementor-element.elementor-element-a17e499.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div > div > div > div.elementor-element.elementor-element-cfc8aea.elementor-align-center.elementor-widget.elementor-widget-button > div > div > a');
		$I->click('body > div.elementor.elementor-492 > div > div > section.elementor-element.elementor-element-a17e499.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div > div > div > div.elementor-element.elementor-element-cfc8aea.elementor-align-center.elementor-widget.elementor-widget-button > div > div > a');
		$I->scrollTo('body > div.elementor.elementor-46409.elementor-location-single.post-33351.product.type-product.status-publish.has-post-thumbnail.product_cat-club.product_cat-elementor-template.product_cat-homepage-grid.product_cat-marketing-plugins.product_cat-miscellaneous-plugins.product_cat-plugins.product_cat-unlimited-sites-license.product_tag-community-board.product_tag-discussion.product_tag-qa.product_tag-question-answer.product_tag-question-answers.member-discount.discount-restricted.first.instock.featured.downloadable.virtual.sold-individually.purchasable.product-type-simple.product > div > div > section.elementor-element.elementor-element-52755b2.elementor-section-stretched.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div.elementor-element.elementor-element-6a9976d.elementor-column.elementor-col-33.elementor-top-column > div > div > section.elementor-element.elementor-element-2faea08.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-inner-section > div > div > div > div > div > div.elementor-element.elementor-element-8f51321.elementor-widget.elementor-widget-woocommerce-product-add-to-cart > div > div > form > button');
		$I->click('body > div.elementor.elementor-46409.elementor-location-single.post-33351.product.type-product.status-publish.has-post-thumbnail.product_cat-club.product_cat-elementor-template.product_cat-homepage-grid.product_cat-marketing-plugins.product_cat-miscellaneous-plugins.product_cat-plugins.product_cat-unlimited-sites-license.product_tag-community-board.product_tag-discussion.product_tag-qa.product_tag-question-answer.product_tag-question-answers.member-discount.discount-restricted.first.instock.featured.downloadable.virtual.sold-individually.purchasable.product-type-simple.product > div > div > section.elementor-element.elementor-element-52755b2.elementor-section-stretched.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-top-section > div > div > div.elementor-element.elementor-element-6a9976d.elementor-column.elementor-col-33.elementor-top-column > div > div > section.elementor-element.elementor-element-2faea08.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default.elementor-section.elementor-inner-section > div > div > div > div > div > div.elementor-element.elementor-element-8f51321.elementor-widget.elementor-widget-woocommerce-product-add-to-cart > div > div > form > button');
		$I->wait(20);
		$I->waitForElementVisible(_CheckoutPage::$Stripe_CreditcardButton,30);
		$I->see('WP-Answers');
		$I->scrollTo(_CheckoutPage::$Stripe_CreditcardButton);

		$I->click(_CheckoutPage::$Stripe_CreditcardButton);
		$I->fillField(_CheckoutPage::$billingEmail_fieldSelector,$email);

		$Val = $I->grabAttributeFrom(_CheckoutPage::$CarNuberiframeSelector,'name');
		$I->switchToIFrame($Val);
		$I->fillField(_CheckoutPage::$CardNumberField, "4111111111111111");
		$I->switchToIFrame();
		$Val1 = $I->grabAttributeFrom(_CheckoutPage::$ExpDateiframeSelector,'name');
		$I->switchToIFrame($Val1);
		$I->fillField(_CheckoutPage::$ExpDateField, '1222');
		$I->switchToIFrame();
		$Val2 = $I->grabAttributeFrom(_CheckoutPage::$CvvNumberiframeSelector,'name');
		$I->switchToIFrame($Val2);
		$I->fillField(_CheckoutPage::$CVVNumberField, '122');
		$I->switchToIFrame();
		$I->click('#place_order');
		$I->wait(5);
		$I->waitForText('Unable to process this payment, please try again or use alternative method.',20);

		$I->see('Unable to process this payment, please try again or use alternative method.');
	}
}
?>