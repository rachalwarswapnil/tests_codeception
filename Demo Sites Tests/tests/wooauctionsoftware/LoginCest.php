<?php

class LoginCest
{
    public function UserShouldLoginToMyAccountArea(AcceptanceTester $I,Page\MyAccount $myAccount)
    {

        $I->amOnPage('/');
        $I->wait(2);
        $I->click($myAccount->myaccountLink);

        $I->waitForElement($myAccount->my_account_text,20);
        $I->fillField($myAccount->emailField,$myAccount->emailValue);
        $I->fillField($myAccount->passwordField,$myAccount->passwordValue);
        $I->click($myAccount->loginBtn);

        $I->wait(2);
        $I->waitForText('From your account dashboard you can view your',20);

        $I->click($myAccount->logoutLink);
        $I->waitForText('Are you sure you want to log out',20);
        $I->click($myAccount->logoutBtn);
        $I->waitForText('My account',20);

    }

    public function UserSouldSeeProductsFromSimpleAuction(AcceptanceTester $I,
                                                          Page\MyAccount $myAccount,
                                                          Page\WooAuctionSoftware $wooAuctionSoftware)
    {
        $I->amOnPage('/');
        $I->wait(2);
        $I->click($myAccount->myaccountLink);

        $I->waitForElement($myAccount->my_account_text,20);
        $I->fillField($myAccount->emailField,$myAccount->emailValue);
        $I->fillField($myAccount->passwordField,$myAccount->passwordValue);
        $I->click($myAccount->loginBtn);

        $I->wait(2);
        $I->waitForText('From your account dashboard you can view your',20);

        $I->click($wooAuctionSoftware->wooAuctionLogo);
        $I->waitForElement($wooAuctionSoftware->simpleAuctionBanenr,20);
        $I->click($wooAuctionSoftware->simpleAuctionBanenr);
        $I->waitForText('Simple Auctions',20);
    }
}