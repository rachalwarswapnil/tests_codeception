<?php

class GDPRCookiePolicyCest
{
    public function UserShouldSeeGDPRCookiePolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->GDPRCookiePolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("These cookies enable the website to provide enhanced functionality and personalization. These cookies are set to provide better services that we have added to our pages.",30);
        $I->see("These cookies enable the website to provide enhanced functionality and personalization. These cookies are set to provide better services that we have added to our pages.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->GDPRCookiePolicyText);

        $I->waitForText("These cookies enable the website to provide enhanced functionality and personalization. These cookies are set to provide better services that we have added to our pages.",30);
        $I->see("These cookies enable the website to provide enhanced functionality and personalization. These cookies are set to provide better services that we have added to our pages.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}