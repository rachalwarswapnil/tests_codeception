<?php

class AboutUs_TemplateCest
{
    public function UserShouldSeeAboutUsTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                   Page\Acceptance\LoginPage $loginPage,
                                                                                   Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->aboutUsCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->phoneValue);
        $I->see($WPLegalpagesPro->streetValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->aboutUsText);

        $I->waitForText($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->phoneValue);
        $I->see($WPLegalpagesPro->streetValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}