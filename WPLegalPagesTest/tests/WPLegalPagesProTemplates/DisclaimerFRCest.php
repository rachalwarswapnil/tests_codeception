<?php

class DisclaimerFRCest
{
    public function UserShouldSeeFrenchDisclaimerTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->selectFrenchLanguage);
        $I->click($WPLegalpagesPro->FrenchdisclaimerCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Les informations répertoriées sur le site ".$WPLegalpagesPro->domainNameValue." sont uniquement destinées à des fins d'information générale.",20);
        $I->see("Les informations répertoriées sur le site ".$WPLegalpagesPro->domainNameValue." sont uniquement destinées à des fins d'information générale.");
        $I->waitForText("Tous les efforts sont faits pour maintenir ".$WPLegalpagesPro->domainNameValue." en bon état de fonctionnement.",20);
        $I->see("Tous les efforts sont faits pour maintenir ".$WPLegalpagesPro->domainNameValue." en bon état de fonctionnement.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->FrenchdisclaimerText);

        $I->waitForText("Les informations répertoriées sur le site ".$WPLegalpagesPro->domainNameValue." sont uniquement destinées à des fins d’information générale.",20);
        $I->see("Les informations répertoriées sur le site ".$WPLegalpagesPro->domainNameValue." sont uniquement destinées à des fins d’information générale.");
        $I->waitForText("Tous les efforts sont faits pour maintenir ".$WPLegalpagesPro->domainNameValue." en bon état de fonctionnement.",20);
        $I->see("Tous les efforts sont faits pour maintenir ".$WPLegalpagesPro->domainNameValue." en bon état de fonctionnement.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}