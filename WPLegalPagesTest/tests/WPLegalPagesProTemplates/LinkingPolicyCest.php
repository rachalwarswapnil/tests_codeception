<?php

class LinkingPolicyCest
{
    public function UserShouldSeeLinkingPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                                   Page\Acceptance\LoginPage $loginPage,
                                                                                                   Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->linkingPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->domainNameValue." welcomes links to this website",30);
        $I->see($WPLegalpagesPro->domainNameValue." welcomes links to this website");
        $I->waitForText("Links pointing to ".$WPLegalpagesPro->domainNameValue." should not be misleading.",30);
        $I->see("Links pointing to ".$WPLegalpagesPro->domainNameValue." should not be misleading.");
        $I->waitForText("Contact Email: ".$WPLegalpagesPro->emailAddressValue.",All Rights Reserved.",20);
        $I->see("Contact Email: ".$WPLegalpagesPro->emailAddressValue.",All Rights Reserved.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->linkingPolicyText);

        $I->waitForText($WPLegalpagesPro->domainNameValue." welcomes links to this website",30);
        $I->see($WPLegalpagesPro->domainNameValue." welcomes links to this website");
        $I->waitForText("Links pointing to ".$WPLegalpagesPro->domainNameValue." should not be misleading.",30);
        $I->see("Links pointing to ".$WPLegalpagesPro->domainNameValue." should not be misleading.");
        $I->waitForText("Contact Email: ".$WPLegalpagesPro->emailAddressValue.",All Rights Reserved.",20);
        $I->see("Contact Email: ".$WPLegalpagesPro->emailAddressValue.",All Rights Reserved.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}