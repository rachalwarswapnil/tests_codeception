<?php

class ReturnsandRefundsPolicy_NoRefundsCest
{
    public function UserShouldSeeReturnsandRefundsPolicy_NoRefundsTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->returnsandRefundsPolicy_NoRefundsCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Thanks for purchasing our products (or subscribing to our services) at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("Thanks for purchasing our products (or subscribing to our services) at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->addressValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);


        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->returnsandRefundsPolicy_NoRefundsText);

        $I->waitForText("Thanks for purchasing our products (or subscribing to our services) at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("Thanks for purchasing our products (or subscribing to our services) at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->addressValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}