<?php

class ReturnsandRefundsPolicy_DigitalGoodsCest
{
    public function UserShouldSeeReturnsandRefundsPolicy_DigitalGoodsTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->returnsandRefundsPolicy_DigitalGoodsCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Thanks for purchasing our products (or subscribing to our services) at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("Thanks for purchasing our products (or subscribing to our services) at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("You are eligible for a full reimbursement within ".$WPLegalpagesPro->returnPeriodValue." calendar days of your purchase. After the ".$WPLegalpagesPro->returnPeriodValue."-day period you will no longer be eligible and won't be able to receive a refund.",20);
        $I->see("You are eligible for a full reimbursement within ".$WPLegalpagesPro->returnPeriodValue." calendar days of your purchase. After the ".$WPLegalpagesPro->returnPeriodValue."-day period you will no longer be eligible and won't be able to receive a refund.");
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->addressValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->returnsandRefundsPolicy_DigitalGoodsText);

        $I->waitForText("Thanks for purchasing our products (or subscribing to our services) at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("Thanks for purchasing our products (or subscribing to our services) at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("You are eligible for a full reimbursement within ".$WPLegalpagesPro->returnPeriodValue." calendar days of your purchase. After the ".$WPLegalpagesPro->returnPeriodValue."-day period you will no longer be eligible and won’t be able to receive a refund.",20);
        $I->see("You are eligible for a full reimbursement within ".$WPLegalpagesPro->returnPeriodValue." calendar days of your purchase. After the ".$WPLegalpagesPro->returnPeriodValue."-day period you will no longer be eligible and won’t be able to receive a refund.");
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->addressValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}