<?php

class GDPRPrivacyPolicyDECest
{
    public function UserShouldSeeGDPRPrivacyPolicyTemplateCreatedInGermanLanguageAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                                             Page\Acceptance\LoginPage $loginPage,
                                                                                                             Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->selectGermanLanguage);
        $I->click($WPLegalpagesPro->GermanGDPRPrivacyPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->domainNameValue." sind verpflichtet",20);
        $I->see($WPLegalpagesPro->domainNameValue." sind verpflichtet");
        $I->waitForText($WPLegalpagesPro->emailAddressValue." kontaktieren.",20);
        $I->see($WPLegalpagesPro->emailAddressValue." kontaktieren.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->GermanGDPRPrivacyPolicyText);

        $I->waitForText($WPLegalpagesPro->domainNameValue." sind verpflichtet",20);
        $I->see($WPLegalpagesPro->domainNameValue." sind verpflichtet");
        $I->waitForText($WPLegalpagesPro->emailAddressValue." kontaktieren.",20);
        $I->see($WPLegalpagesPro->emailAddressValue." kontaktieren.");
        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }


}
