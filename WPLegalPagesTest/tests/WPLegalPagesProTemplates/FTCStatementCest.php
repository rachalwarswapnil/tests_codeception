<?php

class FTCStatementCest
{
    public function UserShouldSeeFTCStatementTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->FTCStatementCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("The disclosure that follows is designed to ensure ".$WPLegalpagesPro->domainNameValue."'s full compliance with the Federal Trade Commission's policy that demands ".$WPLegalpagesPro->domainNameValue." be transparent about any and all affiliate relations ".$WPLegalpagesPro->domainNameValue." may have on this website.");
        $I->see("The disclosure that follows is designed to ensure ".$WPLegalpagesPro->domainNameValue."'s full compliance with the Federal Trade Commission's policy that demands ".$WPLegalpagesPro->domainNameValue." be transparent about any and all affiliate relations ".$WPLegalpagesPro->domainNameValue." may have on this website.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->FTCStatementText);

        $I->waitForText("The disclosure that follows is designed to ensure ".$WPLegalpagesPro->domainNameValue."’s full compliance with the Federal Trade Commission’s policy that demands ".$WPLegalpagesPro->domainNameValue." be transparent about any and all affiliate relations ".$WPLegalpagesPro->domainNameValue." may have on this website.",30);
        $I->see("The disclosure that follows is designed to ensure ".$WPLegalpagesPro->domainNameValue."’s full compliance with the Federal Trade Commission’s policy that demands ".$WPLegalpagesPro->domainNameValue." be transparent about any and all affiliate relations ".$WPLegalpagesPro->domainNameValue." may have on this website.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}