<?php

class CookiesPolicyCest
{
    public function UserShouldSeeCookiesPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->cookiesPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("My Company ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("My Company ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("My Website ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("My Website ".$WPLegalpagesPro->domainNameValue);
        $I->waitForText("Display targeted ads within the ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("Display targeted ads within the ".$WPLegalpagesPro->domainNameValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->cookiesPolicyText);

        $I->waitForText("My Company ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("My Company ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("My Website ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("My Website ".$WPLegalpagesPro->domainNameValue);
        $I->waitForText("Display targeted ads within the ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("Display targeted ads within the ".$WPLegalpagesPro->domainNameValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}