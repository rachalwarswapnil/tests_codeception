<?php

class CCPAtemplateCest
{
    public function UserShouldSeeCCPATemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->CCPAPageCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->businessNameValue." or ".$WPLegalpagesPro->domainNameValue,30);
        $I->see($WPLegalpagesPro->businessNameValue." or ".$WPLegalpagesPro->domainNameValue);
        $I->waitForText("Calling us at ".$WPLegalpagesPro->phoneValue,30);
        $I->see("Calling us at ".$WPLegalpagesPro->phoneValue);
        $I->waitForText("Emailing us at ".$WPLegalpagesPro->emailAddressValue,30);
        $I->see("Emailing us at ".$WPLegalpagesPro->emailAddressValue);
        $I->waitForText("please send an email to ".$WPLegalpagesPro->emailAddressValue.".",30);
        $I->see("please send an email to ".$WPLegalpagesPro->emailAddressValue.".");
        $I->waitForText("Phone: ".$WPLegalpagesPro->phoneValue,30);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->waitForText("Website: ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("Website: ".$WPLegalpagesPro->domainNameValue);
        $I->waitForText("Email: ".$WPLegalpagesPro->emailAddressValue,30);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->CCPAText);

        $I->waitForText($WPLegalpagesPro->businessNameValue." or ".$WPLegalpagesPro->domainNameValue,30);
        $I->see($WPLegalpagesPro->businessNameValue." or ".$WPLegalpagesPro->domainNameValue);
        $I->waitForText("Calling us at ".$WPLegalpagesPro->phoneValue,30);
        $I->see("Calling us at ".$WPLegalpagesPro->phoneValue);
        $I->waitForText("Emailing us at ".$WPLegalpagesPro->emailAddressValue,30);
        $I->see("Emailing us at ".$WPLegalpagesPro->emailAddressValue);
        $I->waitForText("please send an email to ".$WPLegalpagesPro->emailAddressValue.".",30);
        $I->see("please send an email to ".$WPLegalpagesPro->emailAddressValue.".");
        $I->waitForText("Phone: ".$WPLegalpagesPro->phoneValue,30);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->waitForText("Website: ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("Website: ".$WPLegalpagesPro->domainNameValue);
        $I->waitForText("Email: ".$WPLegalpagesPro->emailAddressValue,30);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}