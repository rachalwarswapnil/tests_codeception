<?php

class ConfidentialityDisclosureCest
{
    public function UserShouldSeeConfidentialityDisclosureTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->confidentialityDisclosureCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("The Disclosing Party and the Recipient Party are referred to each as a Party and collectively as the Parties.",30);
        $I->see("The Disclosing Party and the Recipient Party are referred to each as a Party and collectively as the Parties.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->confidentialityDisclosureText);

        $I->waitForText("The Disclosing Party and the Recipient Party are referred to each as a Party and collectively as the Parties.",30);
        $I->see("The Disclosing Party and the Recipient Party are referred to each as a Party and collectively as the Parties.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}