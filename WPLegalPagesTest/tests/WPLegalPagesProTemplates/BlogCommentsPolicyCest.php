<?php

class BlogCommentsPolicyCest
{
    public function UserShouldSeeBlogCommentsPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->blogCommentsPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("We would also like to thank everyone who takes their time out in posting comments on ".$WPLegalpagesPro->businessNameValue);
        $I->see("We would also like to thank everyone who takes their time out in posting comments on ".$WPLegalpagesPro->businessNameValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->blogCommentsPolicyText);

        $I->waitForText("We would also like to thank everyone who takes their time out in posting comments on ".$WPLegalpagesPro->businessNameValue);
        $I->see("We would also like to thank everyone who takes their time out in posting comments on ".$WPLegalpagesPro->businessNameValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}