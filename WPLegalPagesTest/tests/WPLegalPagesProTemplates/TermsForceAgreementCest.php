<?php

class TermsForceAgreementCest
{
    public function UserShouldSeeTerms_ForceAgreementTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                Page\Acceptance\LoginPage $loginPage,
                                                                                Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->terms_ForceAgreementPageCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("PLEASE READ! ".$WPLegalpagesPro->domainNameValue." REQUIRES CONSIDERATION FOR AND AS A CONDITION OF ALLOWING YOU ACCESS.",30);
        $I->see("PLEASE READ! ".$WPLegalpagesPro->domainNameValue." REQUIRES CONSIDERATION FOR AND AS A CONDITION OF ALLOWING YOU ACCESS.");
        $I->waitForText("PRIVACY POLICY OF ".$WPLegalpagesPro->domainNameValue." ARE REQUIRED CONSIDERATIONS FOR ".$WPLegalpagesPro->domainNameValue." GRANTING YOU THE RIGHT TO VISIT, READ OR INTERACT WITH IT.",30);
        $I->see("PRIVACY POLICY OF ".$WPLegalpagesPro->domainNameValue." ARE REQUIRED CONSIDERATIONS FOR ".$WPLegalpagesPro->domainNameValue." GRANTING YOU THE RIGHT TO VISIT, READ OR INTERACT WITH IT.");
        $I->waitForText("TERMS OF USE POLICY AND THE PRIVACY POLICY OF ".$WPLegalpagesPro->domainNameValue.".",30);
        $I->see("TERMS OF USE POLICY AND THE PRIVACY POLICY OF ".$WPLegalpagesPro->domainNameValue.".");
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->streetValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Contact Email: ".$WPLegalpagesPro->emailAddressValue.", All Rights Reserved.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->terms_ForceAgreementText);

        $I->waitForText("PLEASE READ! ".$WPLegalpagesPro->domainNameValue." REQUIRES CONSIDERATION FOR AND AS A CONDITION OF ALLOWING YOU ACCESS.",30);
        $I->see("PLEASE READ! ".$WPLegalpagesPro->domainNameValue." REQUIRES CONSIDERATION FOR AND AS A CONDITION OF ALLOWING YOU ACCESS.");
        $I->waitForText("PRIVACY POLICY OF ".$WPLegalpagesPro->domainNameValue." ARE REQUIRED CONSIDERATIONS FOR ".$WPLegalpagesPro->domainNameValue." GRANTING YOU THE RIGHT TO VISIT, READ OR INTERACT WITH IT.",30);
        $I->see("PRIVACY POLICY OF ".$WPLegalpagesPro->domainNameValue." ARE REQUIRED CONSIDERATIONS FOR ".$WPLegalpagesPro->domainNameValue." GRANTING YOU THE RIGHT TO VISIT, READ OR INTERACT WITH IT.");
        $I->waitForText("TERMS OF USE POLICY AND THE PRIVACY POLICY OF ".$WPLegalpagesPro->domainNameValue.".",30);
        $I->see("TERMS OF USE POLICY AND THE PRIVACY POLICY OF ".$WPLegalpagesPro->domainNameValue.".");
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->streetValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Contact Email: ".$WPLegalpagesPro->emailAddressValue.", All Rights Reserved.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}