<?php

class DigitalGoodsRefundPolicyCest
{
    public function UserShouldSeeDigitalGoodsRefundPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->digitalGoodsRefundPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Depending on the price of the product, ".$WPLegalpagesPro->domainNameValue." may require you to first submit proof that you have submitted a report to the mail service describing the missing item;",20);
        $I->see("Depending on the price of the product, ".$WPLegalpagesPro->domainNameValue." may require you to first submit proof that you have submitted a report to the mail service describing the missing item;");
        $I->waitForText("You have problems that prevent you from downloading the product. ".$WPLegalpagesPro->domainNameValue." recommends that you contact the support team for your browser provider, as ".$WPLegalpagesPro->domainNameValue." ensures that our software can be downloaded with all major browsers, and this problem usually arises from a customer's issue with either their browser, firewall, or network;",30);
        $I->see("You have problems that prevent you from downloading the product. ".$WPLegalpagesPro->domainNameValue." recommends that you contact the support team for your browser provider, as ".$WPLegalpagesPro->domainNameValue." ensures that our software can be downloaded with all major browsers, and this problem usually arises from a customer's issue with either their browser, firewall, or network;");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->digitalGoodsRefundPolicyText);

        $I->waitForText("Depending on the price of the product, ".$WPLegalpagesPro->domainNameValue." may require you to first submit proof that you have submitted a report to the mail service describing the missing item;",20);
        $I->see("Depending on the price of the product, ".$WPLegalpagesPro->domainNameValue." may require you to first submit proof that you have submitted a report to the mail service describing the missing item;");
        $I->waitForText("You have problems that prevent you from downloading the product. ".$WPLegalpagesPro->domainNameValue." recommends that you contact the support team for your browser provider, as ".$WPLegalpagesPro->domainNameValue." ensures that our software can be downloaded with all major browsers, and this problem usually arises from a customer’s issue with either their browser, firewall, or network;");
        $I->see("You have problems that prevent you from downloading the product.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}