<?php

class AntispamCest
{
    public function UserShouldSeeAntispamTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->antispamCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->businessNameValue." ".$WPLegalpagesPro->domainNameValue." has a zero-tolerance spam policy.",30);
        $I->see($WPLegalpagesPro->businessNameValue." ".$WPLegalpagesPro->domainNameValue." has a zero-tolerance spam policy.");
        $I->waitForText($WPLegalpagesPro->domainNameValue." may amend this anti-spam policy at any time by publishing a new version on this website.",20);
        $I->see($WPLegalpagesPro->domainNameValue." may amend this anti-spam policy at any time by publishing a new version on this website.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->antispamText);

        $I->waitForText($WPLegalpagesPro->businessNameValue." ".$WPLegalpagesPro->domainNameValue." has a zero-tolerance spam policy.",30);
        $I->see($WPLegalpagesPro->businessNameValue." ".$WPLegalpagesPro->domainNameValue." has a zero-tolerance spam policy.");
        $I->waitForText($WPLegalpagesPro->domainNameValue." may amend this anti-spam policy at any time by publishing a new version on this website.",20);
        $I->see($WPLegalpagesPro->domainNameValue." may amend this anti-spam policy at any time by publishing a new version on this website.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}