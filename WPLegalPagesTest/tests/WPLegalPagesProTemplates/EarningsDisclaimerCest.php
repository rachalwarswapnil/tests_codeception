<?php

class EarningsDisclaimerCest
{
    public function UserShouldSeeEarningsDisclaimerTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                                   Page\Acceptance\LoginPage $loginPage,
                                                                                                   Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->earningsDisclaimerCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->domainNameValue." does not warrant the performance, effectiveness or applicability of any sites listed or linked to on ".$WPLegalpagesPro->domainNameValue,20);
        $I->see($WPLegalpagesPro->domainNameValue." does not warrant the performance, effectiveness or applicability of any sites listed or linked to on ".$WPLegalpagesPro->domainNameValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->earningsDisclaimerText);

        $I->waitForText($WPLegalpagesPro->domainNameValue." does not warrant the performance, effectiveness or applicability of any sites listed or linked to on ".$WPLegalpagesPro->domainNameValue,20);
        $I->see($WPLegalpagesPro->domainNameValue." does not warrant the performance, effectiveness or applicability of any sites listed or linked to on ".$WPLegalpagesPro->domainNameValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}