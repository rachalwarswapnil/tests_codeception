<?php

class DisclaimerCest
{
    public function UserShouldSeeDisclaimerTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                                   Page\Acceptance\LoginPage $loginPage,
                                                                                                   Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->disclaimerCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Every effort is made to keep ".$WPLegalpagesPro->domainNameValue." up and running smoothly. However,".$WPLegalpagesPro->domainNameValue." takes no responsibility for, and will not be liable for, ".$WPLegalpagesPro->domainNameValue." being temporarily unavailable due to technical issues beyond our control.",30);
        $I->see("Every effort is made to keep ".$WPLegalpagesPro->domainNameValue." up and running smoothly. However,".$WPLegalpagesPro->domainNameValue." takes no responsibility for, and will not be liable for, ".$WPLegalpagesPro->domainNameValue." being temporarily unavailable due to technical issues beyond our control.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->disclaimerText);

        $I->waitForText("Every effort is made to keep ".$WPLegalpagesPro->domainNameValue." up and running smoothly. However,".$WPLegalpagesPro->domainNameValue." takes no responsibility for, and will not be liable for, ".$WPLegalpagesPro->domainNameValue." being temporarily unavailable due to technical issues beyond our control.",30);
        $I->see("Every effort is made to keep ".$WPLegalpagesPro->domainNameValue." up and running smoothly. However,".$WPLegalpagesPro->domainNameValue." takes no responsibility for, and will not be liable for, ".$WPLegalpagesPro->domainNameValue." being temporarily unavailable due to technical issues beyond our control.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}