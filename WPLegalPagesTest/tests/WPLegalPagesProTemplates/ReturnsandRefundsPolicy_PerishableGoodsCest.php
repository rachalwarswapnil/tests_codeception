<?php

class ReturnsandRefundsPolicy_PerishableGoodsCest
{
    public function UserShouldSeeReturnsandRefundsPolicy_PerishableGoodsTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                                                Page\Acceptance\LoginPage $loginPage,
                                                                                                                Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->returnsandRefundsPolicy_PerishableGoodsCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("Due to the nature of our business and products we sell, items that expire sooner than ".$WPLegalpagesPro->returnPeriodValue." from the date of the purchase are not eligible for a refund.",30);
        $I->see("Due to the nature of our business and products we sell, items that expire sooner than ".$WPLegalpagesPro->returnPeriodValue." from the date of the purchase are not eligible for a refund.");
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->addressValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->returnsandRefundsPolicy_PerishableGoodsText);

        $I->waitForText("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("Due to the nature of our business and products we sell, items that expire sooner than ".$WPLegalpagesPro->returnPeriodValue." from the date of the purchase are not eligible for a refund.",30);
        $I->see("Due to the nature of our business and products we sell, items that expire sooner than ".$WPLegalpagesPro->returnPeriodValue." from the date of the purchase are not eligible for a refund.");
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->addressValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}