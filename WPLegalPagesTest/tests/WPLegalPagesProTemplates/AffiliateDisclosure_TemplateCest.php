<?php

class AffiliateDisclosure_TemplateCest
{
    public function UserShouldSeeAffiliateDisclosureTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->affiliateDisclosureCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("The owner of ".$WPLegalpagesPro->domainNameValue." may receive compensation for recommendations made in reference to the products or services on this website.",30);
        $I->see("The owner of ".$WPLegalpagesPro->domainNameValue." may receive compensation for recommendations made in reference to the products or services on this website.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->affiliateDisclosureText);

        $I->waitForText("The owner of ".$WPLegalpagesPro->domainNameValue." may receive compensation for recommendations made in reference to the products or services on this website.",30);
        $I->see("The owner of ".$WPLegalpagesPro->domainNameValue." may receive compensation for recommendations made in reference to the products or services on this website.");
        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}