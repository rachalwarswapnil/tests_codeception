<?php

class AmazonAffiliateDisclosureCest
{
    public function UserShouldSeeAmazonAffiliateDisclosureTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->amazonAffiliateDisclosureCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue." is a participant in the Amazon Services LLC Associates Program",30);
        $I->see($WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue." is a participant in the Amazon Services LLC Associates Program");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->amazonAffiliateDisclosureText);

        $I->waitForText($WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue." is a participant in the Amazon Services LLC Associates Program",30);
        $I->see($WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue." is a participant in the Amazon Services LLC Associates Program");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}