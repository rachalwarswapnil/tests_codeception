<?php

class CaliforniaPrivacyRightsCest
{
    public function UserShouldSeeCaliforniaPrivacyRightsTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                Page\Acceptance\LoginPage $loginPage,
                                                                                Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->californiaPrivacyRightsCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("If you are a California resident, California Civil Code Section 1798.83 permits you to request information regarding the disclosure of your personal information by ".$WPLegalpagesPro->businessNameValue." to third parties for the third parties’ direct marketing purposes. To make such a request, please send an email to ".$WPLegalpagesPro->emailAddressValue,30);
        $I->see("If you are a California resident, California Civil Code Section 1798.83 permits you to request information regarding the disclosure of your personal information by ".$WPLegalpagesPro->businessNameValue." to third parties for the third parties’ direct marketing purposes. To make such a request, please send an email to ".$WPLegalpagesPro->emailAddressValue);
        $I->waitForText($WPLegalpagesPro->businessNameValue." does not share guests’ personal information with other companies or others outside for those parties’ direct marketing use unless a guest elects that we do so.",30);
        $I->see($WPLegalpagesPro->businessNameValue." does not share guests’ personal information with other companies or others outside for those parties’ direct marketing use unless a guest elects that we do so.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->californiaPrivacyRightsText);

        $I->waitForText("If you are a California resident, California Civil Code Section 1798.83 permits you to request information regarding the disclosure of your personal information by ".$WPLegalpagesPro->businessNameValue." to third parties for the third parties’ direct marketing purposes. To make such a request, please send an email to ".$WPLegalpagesPro->emailAddressValue,30);
        $I->see("If you are a California resident, California Civil Code Section 1798.83 permits you to request information regarding the disclosure of your personal information by ".$WPLegalpagesPro->businessNameValue." to third parties for the third parties’ direct marketing purposes. To make such a request, please send an email to ".$WPLegalpagesPro->emailAddressValue);
        $I->waitForText($WPLegalpagesPro->businessNameValue." does not share guests’ personal information with other companies or others outside for those parties’ direct marketing use unless a guest elects that we do so.",30);
        $I->see($WPLegalpagesPro->businessNameValue." does not share guests’ personal information with other companies or others outside for those parties’ direct marketing use unless a guest elects that we do so.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}
