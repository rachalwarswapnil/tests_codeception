<?php

class GDPRPrivacyPolicyFRCest
{
    public function UserShouldSeeGDPRPrivacyPolicyTemplateCreatedInFrenchLanguageAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                             Page\Acceptance\LoginPage $loginPage,
                                                                                             Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->selectFrenchLanguage);
        $I->click($WPLegalpagesPro->FrenchGDPRPrivacyPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->domainNameValue." s'engagent à respecter votre vie privée.",20);
        $I->see($WPLegalpagesPro->domainNameValue." s'engagent à respecter votre vie privée.");
        $I->waitForText("n'hésitez pas à nous contacter à ".$WPLegalpagesPro->emailAddressValue,20);
        $I->see("n'hésitez pas à nous contacter à ".$WPLegalpagesPro->emailAddressValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->FrenchGDPRPrivacyPolicyText);

        $I->waitForText($WPLegalpagesPro->domainNameValue." s’engagent à respecter votre vie privée.",20);
        $I->see($WPLegalpagesPro->domainNameValue." s’engagent à respecter votre vie privée.");
        $I->waitForText("n’hésitez pas à nous contacter à ".$WPLegalpagesPro->emailAddressValue,20);
        $I->see("n’hésitez pas à nous contacter à ".$WPLegalpagesPro->emailAddressValue);
        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}