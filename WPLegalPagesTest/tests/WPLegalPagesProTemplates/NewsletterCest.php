<?php

class NewsletterCest
{
    public function UserShouldSeeNewsletterTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->newsletterCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("We ".$WPLegalpagesPro->businessNameValue.", reserve all copyrights on text or images on the newsletter. The text or images in the newsletter may not be copied or distributed without prior permission of ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("We ".$WPLegalpagesPro->businessNameValue.", reserve all copyrights on text or images on the newsletter. The text or images in the newsletter may not be copied or distributed without prior permission of ".$WPLegalpagesPro->businessNameValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->newsletterText);

        $I->waitForText("We ".$WPLegalpagesPro->businessNameValue.", reserve all copyrights on text or images on the newsletter. The text or images in the newsletter may not be copied or distributed without prior permission of ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("We ".$WPLegalpagesPro->businessNameValue.", reserve all copyrights on text or images on the newsletter. The text or images in the newsletter may not be copied or distributed without prior permission of ".$WPLegalpagesPro->businessNameValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}