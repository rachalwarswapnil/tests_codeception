<?php

class TestimonialsDisclosureCest
{
    public function UserShouldSeeTestimonialsDisclosureTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                                  Page\Acceptance\LoginPage $loginPage,
                                                                                                  Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->testimonialsDisclosureCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Unique experiences and past performances do not guarantee future results!",20);
        $I->see("Unique experiences and past performances do not guarantee future results!");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->testimonialsDisclosureText);

        $I->waitForText("Unique experiences and past performances do not guarantee future results!",20);
        $I->see("Unique experiences and past performances do not guarantee future results!");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}