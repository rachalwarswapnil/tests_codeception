<?php

class MedicalDisclaimerCest
{
    public function UserShouldSeeMedicalDisclaimerTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->medicaldisclaimerCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("The medical information on ".$WPLegalpagesPro->domainNameValue." is provided without any representations or warranties, express or implied. ".$WPLegalpagesPro->domainNameValue." makes no representations or warranties in relation to the medical information on ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("The medical information on ".$WPLegalpagesPro->domainNameValue." is provided without any representations or warranties, express or implied. ".$WPLegalpagesPro->domainNameValue." makes no representations or warranties in relation to the medical information on ".$WPLegalpagesPro->domainNameValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->medicaldisclaimerText);

        $I->waitForText("The medical information on ".$WPLegalpagesPro->domainNameValue." is provided without any representations or warranties, express or implied. ".$WPLegalpagesPro->domainNameValue." makes no representations or warranties in relation to the medical information on ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("The medical information on ".$WPLegalpagesPro->domainNameValue." is provided without any representations or warranties, express or implied. ".$WPLegalpagesPro->domainNameValue." makes no representations or warranties in relation to the medical information on ".$WPLegalpagesPro->domainNameValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}