<?php

class COPPACest
{
    public function UserShouldSeeCOPPATemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->COPPACreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->businessNameValue." is focused on ensuring the security of kids who utilize our site.");
        $I->see($WPLegalpagesPro->businessNameValue." is focused on ensuring the security of kids who utilize our site.");
        $I->waitForText("we encourage the parent to contact us at ".$WPLegalpagesPro->emailAddressValue);
        $I->see("we encourage the parent to contact us at ".$WPLegalpagesPro->emailAddressValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->COPPAText);

        $I->waitForText($WPLegalpagesPro->businessNameValue." is focused on ensuring the security of kids who utilize our site.");
        $I->see($WPLegalpagesPro->businessNameValue." is focused on ensuring the security of kids who utilize our site.");
        $I->waitForText("we encourage the parent to contact us at ".$WPLegalpagesPro->emailAddressValue);
        $I->see("we encourage the parent to contact us at ".$WPLegalpagesPro->emailAddressValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}