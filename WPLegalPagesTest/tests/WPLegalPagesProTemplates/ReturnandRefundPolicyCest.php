<?php

class ReturnandRefundPolicyCest
{
    public function UserShouldSeereturnandRefundPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->returnandRefundPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("The following returns policy applies to all items sold on ".$WPLegalpagesPro->businessNameValue.".",30);
        $I->see("The following returns policy applies to all items sold on ".$WPLegalpagesPro->businessNameValue.".");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->returnandRefundPolicyText);

        $I->waitForText("The following returns policy applies to all items sold on ".$WPLegalpagesPro->businessNameValue.".",30);
        $I->see("The following returns policy applies to all items sold on ".$WPLegalpagesPro->businessNameValue.".");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}