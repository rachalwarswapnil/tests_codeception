<?php

class ExternalLinksPolicyCest
{
    public function UserShouldSeeExternalLinksPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->externalLinksPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText($WPLegalpagesPro->domainNameValue." links to other, external websites that provide information we determine at our discretion contain the most useful information for our program. While many sites provide very useful information, ".$WPLegalpagesPro->domainNameValue." only links to those sites which provide the most useful content.",30);
        $I->see($WPLegalpagesPro->domainNameValue." links to other, external websites that provide information we determine at our discretion contain the most useful information for our program. While many sites provide very useful information, ".$WPLegalpagesPro->domainNameValue." only links to those sites which provide the most useful content.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->externalLinksPolicyText);

        $I->waitForText($WPLegalpagesPro->domainNameValue." links to other, external websites that provide information we determine at our discretion contain the most useful information for our program. While many sites provide very useful information, ".$WPLegalpagesPro->domainNameValue." only links to those sites which provide the most useful content.",30);
        $I->see($WPLegalpagesPro->domainNameValue." links to other, external websites that provide information we determine at our discretion contain the most useful information for our program. While many sites provide very useful information, ".$WPLegalpagesPro->domainNameValue." only links to those sites which provide the most useful content.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}