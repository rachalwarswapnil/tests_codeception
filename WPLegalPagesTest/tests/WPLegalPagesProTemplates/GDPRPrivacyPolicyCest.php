<?php

class GDPRPrivacyPolicyCest
{
    public function UserShouldSeeGDPRPrivacyPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->GDPRPrivacyPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("My Company ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("My Company ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("My Website ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("My Website ".$WPLegalpagesPro->domainNameValue);
        $I->waitForText("If you have any queries related to our data privacy practices or this privacy policy, feel free to contact us at ".$WPLegalpagesPro->emailAddressValue,30);
        $I->see("If you have any queries related to our data privacy practices or this privacy policy, feel free to contact us at ".$WPLegalpagesPro->emailAddressValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->GDPRPrivacyPolicyText);

        $I->waitForText("My Company ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("My Company ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("My Website ".$WPLegalpagesPro->domainNameValue,30);
        $I->see("My Website ".$WPLegalpagesPro->domainNameValue);
        $I->waitForText("If you have any queries related to our data privacy practices or this privacy policy, feel free to contact us at ".$WPLegalpagesPro->emailAddressValue,30);
        $I->see("If you have any queries related to our data privacy practices or this privacy policy, feel free to contact us at ".$WPLegalpagesPro->emailAddressValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}