<?php

class ReturnsandRefundsPolicy_PhysicalGoodsCest
{
    public function UserShouldSeeReturnsandRefundsPolicy_PhysicalGoodsTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                                                Page\Acceptance\LoginPage $loginPage,
                                                                                                                Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->returnsandRefundsPolicy_PhysicalGoodsCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("In order to be eligible for a refund, you have to return the product within ".$WPLegalpagesPro->returnPeriodValue." calendar days of your purchase to the below-mentioned address in the same condition that you receive it and undamaged in any way.",30);
        $I->see("In order to be eligible for a refund, you have to return the product within ".$WPLegalpagesPro->returnPeriodValue." calendar days of your purchase to the below-mentioned address in the same condition that you receive it and undamaged in any way.");
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->addressValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->returnsandRefundsPolicy_PhysicalGoodsText);

        $I->waitForText("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,30);
        $I->see("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("In order to be eligible for a refund, you have to return the product within ".$WPLegalpagesPro->returnPeriodValue." calendar days of your purchase to the below-mentioned address in the same condition that you receive it and undamaged in any way.",30);
        $I->see("In order to be eligible for a refund, you have to return the product within ".$WPLegalpagesPro->returnPeriodValue." calendar days of your purchase to the below-mentioned address in the same condition that you receive it and undamaged in any way.");
        $I->see($WPLegalpagesPro->businessNameValue);
        $I->see($WPLegalpagesPro->addressValue);
        $I->see($WPLegalpagesPro->cityStateZipCOdeValue);
        $I->see($WPLegalpagesPro->countryValue);
        $I->see("Phone: ".$WPLegalpagesPro->phoneValue);
        $I->see("Email: ".$WPLegalpagesPro->emailAddressValue);

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}