<?php

class DisclaimerDECest
{
    public function UserShouldSeeDisclaimerTemplateCreatedInGermanLanguageAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                            Page\Acceptance\LoginPage $loginPage,
                                                                                            Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->selectGermanLanguage);
        $I->click($WPLegalpagesPro->GermandisclaimerCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("Die in ".$WPLegalpagesPro->domainNameValue." enthaltenen Informationen dienen nur der allgemeinen Information.",20);
        $I->see("Die in ".$WPLegalpagesPro->domainNameValue." enthaltenen Informationen dienen nur der allgemeinen Information.");
        $I->waitForText($WPLegalpagesPro->domainNameValue." reibungslos funktioniert.",20);
        $I->see($WPLegalpagesPro->domainNameValue." reibungslos funktioniert.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->GermandisclaimerText);

        $I->waitForText("Die in ".$WPLegalpagesPro->domainNameValue." enthaltenen Informationen dienen nur der allgemeinen Information.",20);
        $I->see("Die in ".$WPLegalpagesPro->domainNameValue." enthaltenen Informationen dienen nur der allgemeinen Information.");
        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}