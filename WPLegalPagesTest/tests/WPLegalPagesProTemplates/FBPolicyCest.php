<?php

class FBPolicyCest
{
    public function UserShouldSeeFBPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                      Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $WPLegalpagesPro->settingsFillFields($I);
        $I->click($WPLegalpagesPro->saveBtn);
        $I->see($WPLegalpagesPro->settingsSavedText);

        $I->click($WPLegalpagesPro->createPageSubMenu);
        $I->click($WPLegalpagesPro->FBPolicyCreateLink);

        $I->switchToIFrame($WPLegalpagesPro->innerIframeId);
        $I->waitForText("We collect information in accordance with this Privacy Policy, and this Privacy Policy only applies to our information collection practices on Facebook.",30);
        $I->see("We collect information in accordance with this Privacy Policy, and this Privacy Policy only applies to our information collection practices on Facebook.");

        $I->switchToFrame();
        $I->click($WPLegalpagesPro->publishBtn);

        $I->see($WPLegalpagesPro->pageSuccesfullyCreated);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->FBPolicyText);

        $I->waitForText("We collect information in accordance with this Privacy Policy, and this Privacy Policy only applies to our information collection practices on Facebook.",30);
        $I->see("We collect information in accordance with this Privacy Policy, and this Privacy Policy only applies to our information collection practices on Facebook.");

        $WPLegalpagesPro->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}