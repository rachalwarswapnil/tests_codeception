<?php
namespace Page\WPLegalPagesPro;

class WPLegalpagesPro
{
    //Wordpress Selectors

    public $wordPressBtn='#wp-admin-bar-site-name > a';
    public $publishPageBtn_1='//*[@id="editor"]/div/div/div/div[1]/div[2]/div[1]/button';
    public $publishPageBtn_2='//*[@id="editor"]/div/div/div/div[3]/div/div/div[1]/div/div/button';
    public $pagesMenuLink='//*[@id="menu-pages"]/a/div[3]';
    public $trashLinkInPages='#wpbody-content > div.wrap > ul > li.trash > a';
    public $emptyTrashBtn='//*[@id="delete_all"]';

    //Template Names Selectors

    public $privacyPolicyText='Privacy Policy';
    public $DMCAText='DMCA';
    public $termsOfUseText='Terms of Use';
    public $CCPAText='CCPA – California Consumer Privacy Act';
    public $californiaPrivacyRightsText='California Privacy Rights';
    public $terms_ForceAgreementText='Terms(forced agreement)';
    public $earningsDisclaimerText='Earnings Disclaimer';
    public $disclaimerText='Disclaimer';
    public $testimonialsDisclosureText='Testimonials Disclosure';
    public $linkingPolicyText='Linking Policy';
    public $refundPolicyText='Refund-Policy';
    public $returnandRefundPolicyText='Return and Refund Policy';
    public $returnsandRefundsPolicy_NoRefundsText='Returns and Refunds Policy: No Refunds';
    public $returnsandRefundsPolicy_DigitalGoodsText='Returns and Refunds Policy: Digital Goods';
    public $returnsandRefundsPolicy_PhysicalGoodsText='Returns and Refunds Policy: Physical Goods';
    public $returnsandRefundsPolicy_PerishableGoodsText='Returns and Refunds Policy: Perishable Goods';
    public $affiliateAgreementText='Affiliate Agreement';
    public $antispamText='Antispam';
    public $FTCStatementText='FTC Statement';
    public $medicaldisclaimerText='Medical Disclaimer';
    public $amazonAffiliateDisclosureText='Amazon Affiliate Disclosure';
    public $doubleDartCookieText='Double Dart Cookie';
    public $externalLinksPolicyText='External Links Policy';
    public $affiliateDisclosureText='Affiliate Disclosure';
    public $FBPolicyText='FB Policy';
    public $aboutUsText='About Us';
    public $digitalGoodsRefundPolicyText='Digital Goods Refund Policy';
    public $COPPAText="COPPA – Children’s Online Privacy Policy";
    public $blogCommentsPolicyText='Blog Comments Policy';
    public $newsletterText='Newsletter : Subscription and Disclaimer';
    public $cookiesPolicyText='Cookies Policy';
    public $GDPRCookiePolicyText='GDPR Cookie Policy';
    public $GDPRPrivacyPolicyText='GDPR Privacy Policy';
    public $confidentialityDisclosureText='Confidentiality Disclosure';
    public $FrenchTermsOfUseText='Terms of Use – FR';
    public $GermanTermsOfUseText='Terms of Use – DE';
    public $FrenchdisclaimerText='Disclaimer – FR';
    public $FrenchGDPRPrivacyPolicyText='GDPR Privacy Policy – FR';
    public $GermandisclaimerText='Disclaimer – DE';
    public $GermanGDPRPrivacyPolicyText='GDPR Privacy Policy – DE';

    public $legalPagesMenu='.toplevel_page_legal-pages';
    public $legalPagesSubMenu='//*[@id="toplevel_page_legal-pages"]/ul/li[3]/a';
    public $wizardSubMenu='//*[@id="toplevel_page_legal-pages"]/ul/li[6]/a';
    public $createOrEditSubMenu='//*[@id="toplevel_page_legal-pages"]/ul/li[7]/a';
    public $trashLegalPage='//*[@id="wpbody-content"]/div[3]/table/tbody/tr/td[5]/a[3]';
    public $movedToTrashText="Legal page moved to trash.";
    public $selectFrenchLanguage='//*[@id="lp_generalid_right"]/div/select/option[2]';
    public $selectGermanLanguage='//*[@id="lp_generalid_right"]/div/select/option[3]';

    //Edit/Create Template Selectors

    public $templateTitleField='//*[@id="lp-title"]';
    public $templateTitleValue='homepage';
    public $editedTemplateTitleValue='Indian-Privacy Policy';
    public $templateNotesField='//*[@id="lp-notes"]';
    public $templateNotesValue='HomePage Value';
    public $descriptionField='//*[@id="content"]';
    public $descriptionValue='This is Demo Template Of Indian-Privacy Policy';
    public $saveBtnForCreatingTemplate='//*[@id="lp_generalid"]/form/p[5]/input[4]';
    public $deleteCreatedTemplate='//*[@id="wpbody-content"]/div[5]/form/table/tbody/tr[41]/td[6]/a[3]';
    public $editCreatedTemplate='//*[@id="wpbody-content"]/div[5]/form/table/tbody/tr[41]/td[6]/a[1]';
    public $editedDescription='This is edited Description For Template Of Indian-Privacy Policy';
    public $editBtn='//*[@id="lp_generalid"]/form/p[5]/input[4]';
    public $deleteEditedTemplate='#wpbody-content > div.wrap > form > table > tbody > tr:nth-child(41) > td:nth-child(6) > a:nth-child(3)';
    public $selectTextField='//*[@id="content-html"]';

   //Create Popups

    public $createPopupSubMenu='//*[@id="toplevel_page_legal-pages"]/ul/li[8]/a';
    public $popupNameField='//*[@id="lp-name"]';
    public $popupNameValue='Indian Privacy';
    public $descriptionForPoupField='//*[@id="content"]';
    public $popUpSaveBtn='//*[@id="lp_generalid"]/form/p[4]/input[3]';
    public $deletePopup='//*[@id="wpbody-content"]/div[5]/table/tbody/tr/td[4]/a[2]';
    public $selectTemplateDropDown='//*[@id="wplp"]';
    public $templateselected='//*[@id="wplp"]/option[2]';
    public $selectorToGrabTemplateShortCode='//*[@id="wplpcode"]';


    //Age Verification

    public $ageVerificationSubMenu='//*[@id="toplevel_page_legal-pages"]/ul/li[9]/a';
    public $enableAgeVerficationRadioBtn='//*[@id="lp_age_verify_form"]/table[1]/tbody/tr[1]/td/fieldset/label[1]/input';
    public $allVisitorsSelectRadioBtn='//*[@id="lp_age_verify_form"]/table[1]/tbody/tr[2]/td/fieldset/label[2]/input';
    public $updateBtn='//*[@id="lp_age_verify_form"]/p/input[3]';
    public $yesBtnForAgeVarifiationPopup='//*[@id="lp_verify_yes"]';
    public $noBtnForAgeVarifiationPopup='//*[@id="lp_verify_no"]';
    public $welcomeToWordpressTextXpath='//*[@id="post-1"]/div[1]/div/p';
    public $disableAgeVerification='//*[@id="lp_age_verify_form"]/table[1]/tbody/tr[1]/td/fieldset/label[2]/input';

    //General Settings Selectors
    public $domainNameField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[2]/td[2]/input";
    public $domainNameValue="http://localhost/wordpress";

    public $businessNameField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[3]/td[2]/input";
    public $businessNameValue="HummingBird";

    public $phoneField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[4]/td[2]/input";
    public $phoneValue='9011001100';

    public $streetField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[5]/td[2]/input";
    public $streetValue="M G Road";

    public $cityStateZipCodeField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[6]/td[2]/input";
    public $cityStateZipCOdeValue="Pune Maharashtra 411027";

    public $countryField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[7]/td[2]/input";
    public $countryValue="India";

    public $emailAddressField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[8]/td[2]/input";
    public $emailAddressValue='swapnil.rachalwar@hbwsl.com';

    public $addressField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[9]/td[2]/input";
    public $addressValue="Baner Pune Maharashtra";

    public $nicheField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[10]/td[2]/input";
    public $nicheValue="Ecommerce";

    public $facebookUrlField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[11]/td[2]/input";
    public $facebookUrlValue="https://www.facebook.com/";

    public $googleUrlField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[12]/td[2]/input";
    public $googleUrlValue="https://www.google.co.in";

    public $twitterUrlField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[13]/td[2]/input";
    public $twitterUrlvalue="https://www.twitter.com";

    public $linkedInUrlField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[14]/td[2]/input";
    public $linkedInUrlValue="https://linkedin.com";

    public $dateField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[15]/td[2]/input";
    public $dateValue="2020/06/29";

    public $durationField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[16]/td[2]/input";
    public $durationValue="1 month";

    public $daysField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[17]/td[2]/input";
    public $daysValue="";

    public $returnPeriodField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[18]/td[2]/input";
    public $returnPeriodValue="15";

    public $refundProcessingPeriodField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[19]/td[2]/input";
    public $refundProcessingPeriodValue="7";

    public $disclosingPartyNameField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[20]/td[2]/input";
    public $disclosingPartyNameValue="HBWSL";

    public $recipientPartyNameField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[21]/td[2]/input";
    public $recipientPartyNameValue='WPeka';

    public $saveBtn="#lp_admin_generalid > table > tbody > tr > td > form > table > tbody > tr:nth-child(29) > td > input:nth-child(1)";
    public $settingsSavedText="Settings Saved.";
    public $createPageSubMenu='#toplevel_page_legal-pages > ul > li:nth-child(4) > a';

    public $innerIframeId='#content_ifr';
    public $publishBtn='#lp_generalid > form > p:nth-child(5) > input.btn.btn-primary.mybtn';
    public $pageSuccesfullyCreated="Page Successfully Created. You can view your page as a normal Page in Pages Menu.";
    public $privacyPolicyPageCreateLink='//*[@id="legalpages1"]';
    public $DMCAPageCreateLink='//*[@id="legalpages2"]';
    public $termsOfUsePageCreateLink='//*[@id="legalpages3"]';
    public $CCPAPageCreateLink='//*[@id="legalpages6"]';
    public $terms_ForceAgreementPageCreateLink='//*[@id="legalpages7"]';
    public $californiaPrivacyRightsCreateLink='//*[@id="legalpages8"]';
    public $earningsDisclaimerCreateLink='//*[@id="legalpages9"]';
    public $disclaimerCreateLink='//*[@id="legalpages10"]';
    public $FrenchdisclaimerCreateLink='//*[@id="legalpages11"]/a';
    public $GermandisclaimerCreateLink='//*[@id="legalpages12"]/a';
    public $testimonialsDisclosureCreateLink='//*[@id="legalpages13"]';
    public $linkingPolicyCreateLink='//*[@id="legalpages14"]';
    public $refundPolicyCreateLink='//*[@id="legalpages15"]';
    public $returnandRefundPolicyCreateLink='//*[@id="legalpages16"]';
    public $returnsandRefundsPolicy_NoRefundsCreateLink='//*[@id="legalpages17"]';
    public $returnsandRefundsPolicy_DigitalGoodsCreateLink='//*[@id="legalpages18"]';
    public $returnsandRefundsPolicy_PhysicalGoodsCreateLink='//*[@id="legalpages19"]';
    public $returnsandRefundsPolicy_PerishableGoodsCreateLink='//*[@id="legalpages20"]';
    public $affiliateAgreementCreateLink='//*[@id="legalpages21"]';
    public $antispamCreateLink='//*[@id="legalpages22"]';
    public $FTCStatementCreateLink='//*[@id="legalpages23"]';
    public $medicaldisclaimerCreateLink='//*[@id="legalpages24"]';
    public $amazonAffiliateDisclosureCreateLink='//*[@id="legalpages25"]';
    public $doubleDartCookieCreateLink='//*[@id="legalpages26"]';
    public $externalLinksPolicyCreateLink='//*[@id="legalpages27"]';
    public $affiliateDisclosureCreateLink='//*[@id="legalpages28"]';
    public $FBPolicyCreateLink='//*[@id="legalpages29"]';
    public $aboutUsCreateLink='//*[@id="legalpages30"]';
    public $digitalGoodsRefundPolicyCreateLink='//*[@id="legalpages31"]';
    public $COPPACreateLink='//*[@id="legalpages32"]';
    public $blogCommentsPolicyCreateLink='//*[@id="legalpages33"]';
    public $newsletterCreateLink='//*[@id="legalpages34"]';
    public $cookiesPolicyCreateLink='//*[@id="legalpages35"]';
    public $GDPRCookiePolicyCreateLink='//*[@id="legalpages36"]';
    public $GDPRPrivacyPolicyCreateLink='//*[@id="legalpages37"]';
    public $FrenchGDPRPrivacyPolicyCreateLink='//*[@id="legalpages38"]/a';
    public $GermanGDPRPrivacyPolicyCreateLink='//*[@id="legalpages39"]/a';
    public $confidentialityDisclosureCreateLink='//*[@id="legalpages40"]';
    public $termsOfUseFRPageCreateLink='//*[@id="legalpages4"]/a';
    public $termsOfUseGermanPageCreateLink='//*[@id="legalpages5"]/a';
    public $DMCAPageTitleText="Digital Millennium Copyright Act Policy";

    public function settingsFillFields($I)
    {
        $I->fillField($this->domainNameField,$this->domainNameValue);
        $I->fillField($this->businessNameField,$this->businessNameValue);
        $I->fillField($this->phoneField,$this->phoneValue);
        $I->fillField($this->streetField,$this->streetValue);
        $I->fillField($this->cityStateZipCodeField,$this->cityStateZipCOdeValue);
        $I->fillField($this->countryField,$this->countryValue);
        $I->fillField($this->emailAddressField,$this->emailAddressValue);
        $I->fillField($this->addressField,$this->addressValue);
        $I->fillField($this->nicheField,$this->nicheValue);
        $I->fillField($this->facebookUrlField,$this->facebookUrlValue);
        $I->fillField($this->googleUrlField,$this->googleUrlValue);
        $I->fillField($this->twitterUrlField,$this->twitterUrlvalue);
        $I->fillField($this->linkedInUrlField,$this->linkedInUrlValue);
        $I->fillField($this->dateField,$this->dateValue);
        $I->fillField($this->durationField,$this->durationValue);
        $I->fillField($this->daysField,$this->dateValue);
        $I->fillField($this->returnPeriodField,$this->returnPeriodValue);
        $I->fillField($this->refundProcessingPeriodField,$this->refundProcessingPeriodValue);
        $I->fillField($this->disclosingPartyNameField,$this->disclosingPartyNameValue);
        $I->fillField($this->recipientPartyNameField,$this->recipientPartyNameValue);

    }

    public function moveTemplateToTrash($I)
    {
        $I->click($this->wordPressBtn);
        $I->wait(1);
        $I->seeElement($this->legalPagesMenu);
        $I->click($this->legalPagesMenu);
        $I->click($this->legalPagesSubMenu);
        $I->click($this->trashLegalPage);

        $I->see($this->movedToTrashText);
        $I->click($this->pagesMenuLink);
        $I->click($this->trashLinkInPages);
        $I->click($this->emptyTrashBtn);

    }

    public function movePageToTrash($I)
    {
        $I->click($this->wordPressBtn);
        $I->click($this->pagesMenuLink);
        $I->click($this->termsAndConditionsText);
        $I->click($this->moveToTrashBtn);
        $I->waitForElement($this->trashLinkInPages);
        $I->click($this->trashLinkInPages);
        $I->click($this->emptyTrashBtn);

    }

    //Widgets selectors


    //Terms And Conditions Wizard
    public $termsAndConditionsCreateBtn='/html/body/div[1]/form/div/div[1]/div[2]/button';

    public $domainNameFieldForWizard='/html/body/div[1]/form/div/div[1]/div/input';
    public $businessNameFieldForWizard='/html/body/div[1]/form/div/div[2]/div/input';
    public $streetFieldForWizard='/html/body/div[1]/form/div/div[3]/div/input';
    public $cityStateZipCOdeFieldForWizard='/html/body/div[1]/form/div/div[4]/div/input';
    public $countryFieldForWizard='/html/body/div[1]/form/div/div[5]/div/input';
    public $emailAddressFieldForWizard='/html/body/div[1]/form/div/div[6]/div/input';
    public $emailAddressFieldForWizardCaliforniaPolicy='/html/body/div[1]/form/div/div[4]/div/input';
    public $phoneFieldForWizard='/html/body/div[1]/form/div/div[3]/div/input';

    public $settingsContinueBtn='//*[@id="page-settings"]';
    public $sectionContinueBtn='//*[@id="page-sections"]';
    public $createPageBtn='/html/body/div[1]/form/p/button[1]';

    public $termsAndConditionsText='Terms and Conditions';
    public $moveToTrashBtn='//*[@id="editor"]/div/div/div/div[4]/div/div[3]/div[1]/div[3]/button';

    //target audience Checkbox
    public $targetAudienceBaner='//*[@id="target_audience"]/label';
    public $targetAudienceDropDownArrow='//*[@id="target_audience"]/label/span';
    public $keepAudieceOpen_radioBtn='//*[@id="common_target_audience_open"]';
    public $endConsumersOnly_radioBtn='//*[@id="common_target_audience_consumers"]';
    public $nonConsumersOnly_radioBtn='//*[@id="common_target_audience_non_consumers"]';
    public $excludeCountries_checkBox='//*[@id="exclude_geographies"]';

    //ContentRights
    public $contentRightsBaner='//*[@id="content_rights"]/label';
    public $allRightsReserved_radioBtn='//*[@id="content_all_rights_reserved"]';
    public $addtionaDescriptionToexplain_checkBox='//*[@id="content_all_rights_meaning"]';
    public $someRightsReserved_radioBtn='//*[@id="content_some_rights_reserved"]';

   //Common Provisions
    public $commonProvisionsBaner='//*[@id="common_provisions"]/label';
    public $specificallyIntegrateThePrivacyPloicy='//*[@id="common_provisions_privacy_integrate"]';
    public $specifyTheGoverningLaw='//*[@id="common_provisions_law_governing_specify"]';
    public $ukUsers_CheckBox='//*[@id="common_provisions_law_jurisdiction_venue_uk_users"]';
    public $usUsers_CheckBox='//*[@id="common_provisions_law_jurisdiction_venue_us_users"]';
    public $youNotifyPeopleOneMonth='//*[@id="common_provisions_document_prior_notify"]';
    public $B2BAddition='//*[@id="common_provisions_severability_advanced_b2b_statement"]';
    public $suggestedWording='//*[@id="common_provisions_us_provision"]';

    //Age Restrictions
    public $ageRestrictionsBanner='//*[@id="age_restrictions"]/label';
    public $usersMayAccesTheApplicationEnterAge_radioBtn='//*[@id="age_restrictions_clause_may"]';
    public $Advancedbanner='//*[@id="age_restrictions_advanced"]';
    public $minorsMayAccessTheApplication_CheckBox='//*[@id="age_restrictions_advanced_minor"]';
    public $setAgeLimitField='//*[@id="age_restrictions_clause_may_age"]';
    public $setAgeLimitValue='21';

    //disclaimers of warrenties,limitations
    public $disclaimersOfWarrantiesBaner='//*[@id="disclaimers_limitations_indemnity"]/label';
    public $addressEU_checkBox='//*[@id="disclaimers_limitations_indemnity_eu_practice_address_eu"]';
    public $suggestedAdditions_CheckBox='//*[@id="disclaimers_limitations_indemnity_general_address_general"]';

    public function termsAndConditionsFillFields($I)
    {
        $I->waitForElement($this->domainNameFieldForWizard,20);
        $I->fillField($this->domainNameFieldForWizard,$this->domainNameValue);
        $I->fillField($this->businessNameFieldForWizard,$this->businessNameValue);
        $I->fillField($this->streetFieldForWizard,$this->streetValue);
        $I->fillField($this->cityStateZipCOdeFieldForWizard,$this->cityStateZipCOdeValue);
        $I->fillField($this->countryFieldForWizard,$this->countryValue);
        $I->fillField($this->emailAddressFieldForWizard,$this->emailAddressValue);

        $I->click($this->settingsContinueBtn);//Continue button

    }


    //California Policy CCPA Wizard

    public $californiaPolicyWizardCrateBtn='/html/body/div[1]/form/div/div[1]/div[3]/button';
    public $californiaPolicyPageText='Privacy Notice For California Residents';
    public $privacyPolicyPageText='Privacy Policy';

    //INFORMATION WEBSITE COLLECTS
    public $identifiers_CheckBox='//*[@id="identifiers"]';
    public $personalInformationCategories_CheckBox='//*[@id="personal_information"]';
    public $protectedClassificationCharacteristics_CheckBox='//*[@id="protected_classification"]';
    public $commercialInformation_CheckBox='//*[@id="commercial_information"]';
    public $biometricInformation_CheckBox='//*[@id="biometric_information"]';
    public $internetOrOtherSimilarNetworkActivities_checkBox='//*[@id="internet_activity"]';
    public $geolocationData_checkBox='//*[@id="geolocation_data"]';
    public $sensoryData_checkBox='//*[@id="sensory_data"]';
    public $professionalOrEmployeeRelatedInformation_checkBox='//*[@id="professional_information"]';
    public $nonPlublicEducationInformation_check_Box='//*[@id="education_information"]';
    public $inferencesDrawnFromOtherPersonalInformation_checkBox='//*[@id="inference_information"]';

    //cookie Audit Table
    public $cookieAuditTableBaner='//*[@id="cookie_audit_table"]/label';

    public function CaliforniaPolicyFillFields($I)
    {
        $I->waitForElement($this->domainNameFieldForWizard,20);
        $I->fillField($this->domainNameFieldForWizard,$this->domainNameValue);
        $I->fillField($this->businessNameFieldForWizard,$this->businessNameValue);
        $I->fillfield($this->phoneFieldForWizard,$this->phoneValue);
        $I->fillField($this->emailAddressFieldForWizardCaliforniaPolicy,$this->emailAddressValue);

        $I->click($this->settingsContinueBtn);//Continue button

    }

    public function movePageToTrashForCCPAWizard($I)
    {
        $I->click($this->wordPressBtn);
        $I->click($this->pagesMenuLink);
        $I->click($this->californiaPolicyPageText);
        $I->click($this->moveToTrashBtn);
        $I->waitForElement($this->trashLinkInPages);
        $I->click($this->trashLinkInPages);
        $I->click($this->emptyTrashBtn);

    }

    //Privacy Policy Wizard
    public $privacyPolicyCreateBtn='/html/body/div[1]/form/div/div[1]/div[1]/button';
    public $natureAndSizeOfTheBusiness_CheckBox='//*[@id="personal_info_business"]';
    public $natureAndSizeOfTheAdvertisingInventory_CheckBox='//*[@id="personal_info_advertising"]';
    public $byContactUs_CheckBox='//*[@id="optout_info_phone"]';
    public $byCotactUs_CheckBox='//*[@id="contact_info_phone"]';
    public $no_AllowThirdPartyService_radioBtn='//*[@id="allow_third_party_no"]';
    public $no_ShareInformationWithThirdParty_radioBtn='//*[@id="allow_third_party_no"]';
    public $no_DoYouUseCookies_radioBtn='//*[@id="use_of_cookies_no"]';
    public $no_implimentThirdPartyCookies_radioBtn='//*[@id="third_party_cookies_no"]';
    public $no_WebsiteContainLinksToOtherSites_radioBtn='//*[@id="website_links_no"]';
    public $no_Subpoena_radioBtn='//*[@id="disclose_of_info_no"]';

    public function PrivacyPolicyFillFields($I)
    {
        $I->waitForElement($this->domainNameFieldForWizard,20);
        $I->fillField($this->domainNameFieldForWizard,$this->domainNameValue);
        $I->fillField($this->businessNameFieldForWizard,$this->businessNameValue);
        $I->fillfield($this->phoneFieldForWizard,$this->phoneValue);
        $I->fillField($this->emailAddressFieldForWizardCaliforniaPolicy,$this->emailAddressValue);

        $I->click($this->settingsContinueBtn);//Continue button

    }

    public function movePageToTrashForPrivacyPolicyWizard($I)
    {
        $I->click($this->wordPressBtn);
        $I->click($this->pagesMenuLink);
        $I->click($this->privacyPolicyPageText);
        $I->click($this->moveToTrashBtn);
        $I->waitForElement($this->trashLinkInPages);
        $I->click($this->trashLinkInPages);
        $I->click($this->emptyTrashBtn);

    }

    //Returns And Refunds Wizards

    public $returnsAndRefundsWizardsCreateBtn='/html/body/div[1]/form/div/div[1]/div[4]/button';
    public $returnsAndRefundsPolicyPageText='Returns and Refunds Policy';
    public $emailAddressFieldForReturnsAndRefundsWizard='/html/body/div[1]/form/div/div[7]/div/input';
    public $phoneFieldForReturnsAndRefundsWizard='/html/body/div[1]/form/div/div[6]/div/input';

    public function ReturnsAndRefundsFillFields($I)
    {
        $I->waitForElement($this->domainNameFieldForWizard,20);
        $I->fillField($this->domainNameFieldForWizard,$this->domainNameValue);
        $I->fillField($this->businessNameFieldForWizard,$this->businessNameValue);
        $I->fillField($this->streetFieldForWizard,$this->streetValue);
        $I->fillField($this->cityStateZipCOdeFieldForWizard,$this->cityStateZipCOdeValue);
        $I->fillField($this->countryFieldForWizard,$this->countryValue);
        $I->fillField($this->phoneFieldForReturnsAndRefundsWizard,$this->phoneValue);
        $I->fillField($this->emailAddressFieldForReturnsAndRefundsWizard,$this->emailAddressValue);

        $I->click($this->settingsContinueBtn);//Continue button

    }

    public function movePageToTrashForReturnsAndRefundsWizard($I)
    {
        $I->click($this->wordPressBtn);
        $I->click($this->pagesMenuLink);
        $I->click($this->returnsAndRefundsPolicyPageText);
        $I->click($this->moveToTrashBtn);
        $I->waitForElement($this->trashLinkInPages);
        $I->click($this->trashLinkInPages);
        $I->click($this->emptyTrashBtn);

    }
}