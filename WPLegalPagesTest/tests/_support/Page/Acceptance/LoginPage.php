<?php
namespace Page\Acceptance;

class LoginPage
{
    public $userLoginField='#user_login';
    public $userPasswordField='#user_pass';
    public $loginBtn='#wp-submit';
    public $wordpressProfile='//*[@id="wp-admin-bar-my-account"]/a/span';
    public $logOutLink='//*[@id="wp-admin-bar-logout"]/a';

    public function userLogin($I)
    {
        $I->amOnPage('/wp-admin');
        $I->wait(2);
        $I->fillField($this->userLoginField,'root');
        $I->fillField($this->userPasswordField,'admin123');
        $I->click($this->loginBtn);

    }

    public function userLogout($I)
    {
        $I->moveMouseOver($this->wordpressProfile);
        $I->click($this->logOutLink);
        $I->wait(2);
    }
}