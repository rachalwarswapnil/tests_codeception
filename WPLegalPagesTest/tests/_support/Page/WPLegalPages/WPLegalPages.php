<?php
namespace Page\WPLegalPages;

class WPLegalPages
{
    public $wordPressBtn='#wp-admin-bar-site-name > a';

    public $privacyPolicyText='Privacy Policy';
    public $DMCAText='DMCA';
    public $termsOfUseText='Terms of Use';
    public $CCPAText='CCPA – California Consumer Privacy Act';
    public $FrenchTermsOfUseText='Terms of Use – FR';
    public $GermanTermsOfUseText='Terms of Use – DE';

    public $legalPagesMenu='.toplevel_page_legal-pages';
    public $legalPagesSubMenu='//*[@id="toplevel_page_legal-pages"]/ul/li[3]/a';
    public $trashLegalPage='//*[@id="wpbody-content"]/div[3]/table/tbody/tr/td[5]/a[3]';
    public $movedToTrashText="Legal page moved to trash.";
    public $selectFrenchLanguage='//*[@id="lp_generalid_right"]/div/select/option[2]';
    public $selectGermanLanguage='//*[@id="lp_generalid_right"]/div/select/option[3]';


    //General Settings Selectors
    public $domainNameField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[2]/td[2]/input";
    public $domainNameValue="http://localhost/wordpress";

    public $businessNameField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[3]/td[2]/input";
    public $businessNameValue="HummingBird";

    public $phoneField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[4]/td[2]/input";
    public $phoneValue='9011001100';

    public $streetField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[5]/td[2]/input";
    public $streetValue="M G Road";

    public $cityStateZipCodeField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[6]/td[2]/input";
    public $cityStateZipCOdeValue="Pune Maharashtra 411027";

    public $countryField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[7]/td[2]/input";
    public $countryValue="India";

    public $emailAddressField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[8]/td[2]/input";
    public $emailAddressValue='swapnil.rachalwar@hbwsl.com';

    public $addressField="//*[@id=\"lp_admin_generalid\"]/table/tbody/tr/td/form/table/tbody/tr[9]/td[2]/input";
    public $addressValue="Baner Pune Maharashtra";

    public $saveBtn='#lp_admin_generalid > table > tbody > tr > td > form > table > tbody > tr:nth-child(12) > td > input:nth-child(1)';
    public $settingsSavedText="Settings Saved.";
    public $createPageSubMenu='#toplevel_page_legal-pages > ul > li:nth-child(4) > a';

    public $innerIframeId='#content_ifr';
    public $publishBtn='#lp_generalid > form > p:nth-child(5) > input.btn.btn-primary.mybtn';

    public $pageSuccesfullyCreated="Page Successfully Created. You can view your page as a normal Page in Pages Menu.";
    public $privacyPolicyPageCreateLink='//*[@id="legalpages1"]';
    public $DMCAPageCreateLink='//*[@id="legalpages2"]';
    public $termsOfUsePageCreateLink='//*[@id="legalpages3"]';
    public $CCPAPageCreateLink='//*[@id="legalpages6"]';
    public $termsOfUseFRPageCreateLink='//*[@id="legalpages4"]/a';
    public $termsOfUseGermanPageCreateLink='//*[@id="legalpages5"]/a';
    public $DMCAPageTitleText="Digital Millennium Copyright Act Policy";


    public function settingsFillFields($I)
    {
        $I->fillField($this->domainNameField,$this->domainNameValue);
        $I->fillField($this->businessNameField,$this->businessNameValue);
        $I->fillField($this->phoneField,$this->phoneValue);
        $I->fillField($this->streetField,$this->streetValue);
        $I->fillField($this->cityStateZipCodeField,$this->cityStateZipCOdeValue);
        $I->fillField($this->countryField,$this->countryValue);
        $I->fillField($this->emailAddressField,$this->emailAddressValue);
        $I->fillField($this->addressField,$this->addressValue);

    }

    public function moveTemplateToTrash($I)
    {
        $I->click($this->wordPressBtn);
        $I->wait(2);
        $I->seeElement($this->legalPagesMenu);
        $I->click($this->legalPagesMenu);
        $I->click($this->legalPagesSubMenu);
        $I->click($this->trashLegalPage);

        $I->see($this->movedToTrashText);

    }
}