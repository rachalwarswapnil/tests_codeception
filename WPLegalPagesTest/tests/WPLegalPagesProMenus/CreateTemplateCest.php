<?php

class CreateTemplateCest
{

    public function UserShouldSeeTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                            Page\Acceptance\LoginPage $loginPage,
                                                                            Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {

        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->createOrEditSubMenu);
        $I->fillField($WPLegalpagesPro->templateTitleField, $WPLegalpagesPro->templateTitleValue);
        $I->fillField($WPLegalpagesPro->templateNotesField, $WPLegalpagesPro->templateNotesValue);

        $I->click($WPLegalpagesPro->saveBtnForCreatingTemplate);

        $I->waitForText("Template Successfully Created.", 20);
        $I->see("Template Successfully Created.");

        $I->click($WPLegalpagesPro->deleteCreatedTemplate);
        $I->seeInPopup('Template will be permanently deleted. Are you sure you want to delete?');
        $I->acceptPopup();

        $I->wait(2);
        $loginPage->userLogout($I);
    }


    public function UserShouldEditCreatedTemplate(AcceptanceTester $I,
                                                  Page\Acceptance\LoginPage $loginPage,
                                                  Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {

        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->createOrEditSubMenu);
        $I->fillField($WPLegalpagesPro->templateTitleField, $WPLegalpagesPro->templateTitleValue);
        $I->fillField($WPLegalpagesPro->templateNotesField, $WPLegalpagesPro->templateNotesValue);
        $I->click($WPLegalpagesPro->selectTextField);
        $I->fillField($WPLegalpagesPro->descriptionField,$WPLegalpagesPro->descriptionValue);
        $I->click($WPLegalpagesPro->saveBtnForCreatingTemplate);

        $I->waitForText("Template Successfully Created.", 20);
        $I->see("Template Successfully Created.");

        $I->click($WPLegalpagesPro->editCreatedTemplate);
        $I->fillField($WPLegalpagesPro->descriptionField,$WPLegalpagesPro->editedDescription);
        $I->click($WPLegalpagesPro->editBtn);
        $loginPage->userLogout($I);
    }
}