<?php

class AgeVerificationCest
{

    public function UserShouldSeeSiteContentsIfHeVarifiesHisAgeByClickingYes(AcceptanceTester $I,
                                                                            Page\Acceptance\LoginPage $loginPage,
                                                                            Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {

        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->ageVerificationSubMenu);
        $I->click($WPLegalpagesPro->enableAgeVerficationRadioBtn);
        $I->click($WPLegalpagesPro->allVisitorsSelectRadioBtn);
        $I->click($WPLegalpagesPro->updateBtn);

        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->see('You must be atleast 21 years of age to visit this site.');
        $I->click($WPLegalpagesPro->yesBtnForAgeVarifiationPopup);

        $I->see('Welcome to WordPress.');
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->ageVerificationSubMenu);
        $I->click($WPLegalpagesPro->disableAgeVerification);
        $I->click($WPLegalpagesPro->updateBtn);
        $loginPage->userLogout($I);
    }

    public function UserShouldDoesNotSeeSiteContentsIfHeClicksNoAtAgeVarificationPopup(AcceptanceTester $I,
                                                                                       Page\Acceptance\LoginPage $loginPage,
                                                                                       Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {

        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->ageVerificationSubMenu);
        $I->click($WPLegalpagesPro->enableAgeVerficationRadioBtn);
        $I->click($WPLegalpagesPro->allVisitorsSelectRadioBtn);
        $I->click($WPLegalpagesPro->updateBtn);

        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->see('You must be atleast 21 years of age to visit this site.');
        $I->click($WPLegalpagesPro->noBtnForAgeVarifiationPopup);

        $I->see('We are Sorry. You are not of valid age.');
        $I->dontSee($WPLegalpagesPro->welcomeToWordpressTextXpath);
        $I->wait(1);
        $I->amOnPage('/wp-admin');
        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->ageVerificationSubMenu);
        $I->click($WPLegalpagesPro->disableAgeVerification);
        $I->click($WPLegalpagesPro->updateBtn);
        $loginPage->userLogout($I);
    }
}
