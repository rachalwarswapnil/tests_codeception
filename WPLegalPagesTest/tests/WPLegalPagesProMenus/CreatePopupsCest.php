<?php

class CreatePopupsCest
{

    public function UserShouldSeePopupCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                            Page\Acceptance\LoginPage $loginPage,
                                                                            Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {

        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->createPopupSubMenu);
        $I->fillField($WPLegalpagesPro->popupNameField,$WPLegalpagesPro->popupNameValue);
        $I->click($WPLegalpagesPro->selectTextField);
        $I->fillField($WPLegalpagesPro->descriptionForPoupField,$WPLegalpagesPro->descriptionValue);
        $I->click($WPLegalpagesPro->popUpSaveBtn);

        $I->see("Popup Successfully Created.");

        $I->click($WPLegalpagesPro->deletePopup);
        $I->seeInPopup('Popup will be permanently deleted. Are you sure you want to delete?');
        $I->acceptPopup();

        $loginPage->userLogout($I);
    }

    public function UserShouldSeePopupCreatedWithTemplateShortCode(AcceptanceTester $I,
                                                                         Page\Acceptance\LoginPage $loginPage,
                                                                         Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {

        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->createPopupSubMenu);
        $I->click($WPLegalpagesPro->selectTemplateDropDown);
        $I->click($WPLegalpagesPro->templateselected);
        $ShortCodeOFTemplate = $I->grabTextFrom($WPLegalpagesPro->selectorToGrabTemplateShortCode);

        $I->fillField($WPLegalpagesPro->popupNameField,$WPLegalpagesPro->popupNameValue);
        $I->click($WPLegalpagesPro->selectTextField);
        $I->wait(1);
        $I->fillField($WPLegalpagesPro->descriptionForPoupField,$ShortCodeOFTemplate);

        $I->click($WPLegalpagesPro->popUpSaveBtn);
        $I->see("Popup Successfully Created.");

        $I->click($WPLegalpagesPro->deletePopup);
        $I->seeInPopup('Popup will be permanently deleted. Are you sure you want to delete?');
        $I->acceptPopup();
        $loginPage->userLogout($I);

    }

}