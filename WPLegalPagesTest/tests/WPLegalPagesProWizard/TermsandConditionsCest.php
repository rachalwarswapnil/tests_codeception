<?php

class TermsandConditionsCest
{
    /*
        public function UserShouldSeeTermsandConditionsWizardCreatedAsPerTargetAudianceForKeepAudienceOpen(AcceptanceTester $I,
                                                                                                           Page\Acceptance\LoginPage $loginPage,
                                                                                                           Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
        {
            $loginPage->userLogin($I);

            $I->seeElement($WPLegalpagesPro->legalPagesMenu);
            $I->click($WPLegalpagesPro->legalPagesMenu);

            $I->click($WPLegalpagesPro->wizardSubMenu);
            $I->waitForText("Terms and Conditions", 20);
            $I->see("Terms and Conditions");
            $I->wait(2);
            $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

            $WPLegalpagesPro->termsAndConditionsFillFields($I);

            $I->click($WPLegalpagesPro->targetAudienceBaner);
            $I->click($WPLegalpagesPro->keepAudieceOpen_radioBtn);
            $I->click($WPLegalpagesPro->sectionContinueBtn);

            $I->waitForText("Please note that some provisions in these Terms may only be applicable to certain categories of Users.", 20);
            $I->see("Please note that some provisions in these Terms may only be applicable to certain categories of Users.");
            $I->waitForText("There are no restrictions for Users in terms of being Business/Commercial Users or Consumers;", 30);
            $I->see("There are no restrictions for Users in terms of being Business/Commercial Users or Consumers;");

            $I->click($WPLegalpagesPro->createPageBtn);
            $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
            $I->click($WPLegalpagesPro->publishPageBtn_1);
            $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
            $I->click($WPLegalpagesPro->publishPageBtn_2);

            $I->wait(2);
            $I->click($WPLegalpagesPro->wordPressBtn);
            $I->click($WPLegalpagesPro->termsAndConditionsText);

            $I->waitForText("Please note that some provisions in these Terms may only be applicable to certain categories of Users.", 20);
            $I->see("Please note that some provisions in these Terms may only be applicable to certain categories of Users.");
            $I->waitForText("There are no restrictions for Users in terms of being Business/Commercial Users or Consumers;", 30);
            $I->see("There are no restrictions for Users in terms of being Business/Commercial Users or Consumers;");

            $WPLegalpagesPro->movePageToTrash($I);
            $loginPage->userLogout($I);
        }

        public
        function UserShouldSeeTermsandConditionsWizardCreatedAsPerTargetAudianceForEndConsumersOnly(AcceptanceTester $I,
                                                                                                    Page\Acceptance\LoginPage $loginPage,
                                                                                                    Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
        {
            $loginPage->userLogin($I);

            $I->seeElement($WPLegalpagesPro->legalPagesMenu);
            $I->click($WPLegalpagesPro->legalPagesMenu);

            $I->click($WPLegalpagesPro->wizardSubMenu);
            $I->waitForText("Terms and Conditions", 20);
            $I->see("Terms and Conditions");
            $I->wait(2);
            $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

            $WPLegalpagesPro->termsAndConditionsFillFields($I);
            $I->wait(2);
            $I->click($WPLegalpagesPro->targetAudienceBaner);
            $I->click($WPLegalpagesPro->endConsumersOnly_radioBtn);
            $I->click($WPLegalpagesPro->sectionContinueBtn);

            $I->waitForText("This Website/Service is only intended for Consumers.", 20);
            $I->see("This Website/Service is only intended for Consumers.");
            $I->waitForText("Users must qualify as Consumers;", 20);
            $I->see("Users must qualify as Consumers;");

            $I->click($WPLegalpagesPro->createPageBtn);
            $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
            $I->click($WPLegalpagesPro->publishPageBtn_1);
            $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
            $I->click($WPLegalpagesPro->publishPageBtn_2);

            $I->wait(2);
            $I->click($WPLegalpagesPro->wordPressBtn);
            $I->click($WPLegalpagesPro->termsAndConditionsText);

            $I->waitForText("This Website/Service is only intended for Consumers.", 20);
            $I->see("This Website/Service is only intended for Consumers.");
            $I->waitForText("Users must qualify as Consumers;", 20);
            $I->see("Users must qualify as Consumers;");

            $WPLegalpagesPro->movePageToTrash($I);
            $loginPage->userLogout($I);
        }


            public function UserShouldSeeTermsandConditionsWizardCreatedAsPerTargetAudianceForNonConsumersOnly(AcceptanceTester $I,
                                                                                                               Page\Acceptance\LoginPage $loginPage,
                                                                                                               Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {
                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);

                $I->click($WPLegalpagesPro->targetAudienceBaner);
                $I->click($WPLegalpagesPro->nonConsumersOnly_radioBtn);
                $I->click($WPLegalpagesPro->sectionContinueBtn);

                $I->waitForText("This Website/Service is only intended for Users who do not qualify as Consumers, such as Business Users.",30);
                $I->see("This Website/Service is only intended for Users who do not qualify as Consumers, such as Business Users.");
                $I->waitForText("Users may not qualify as Consumers;",20);
                $I->see("Users may not qualify as Consumers;");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("This Website/Service is only intended for Users who do not qualify as Consumers, such as Business Users.",30);
                $I->see("This Website/Service is only intended for Users who do not qualify as Consumers, such as Business Users.");
                $I->waitForText("Users may not qualify as Consumers;",20);
                $I->see("Users may not qualify as Consumers;");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);
            }



            public function UserShouldSeeTermsandConditionsWizardCreatedAsPerTargetAudianceForNonConsumersOnlywithExludeCountriesCheckBoxSelected(AcceptanceTester $I,
                                                                                                               Page\Acceptance\LoginPage $loginPage,
                                                                                                               Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {
                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);
                $I->wait(2);
                $I->click($WPLegalpagesPro->targetAudienceBaner);
                $I->click($WPLegalpagesPro->nonConsumersOnly_radioBtn);
                $I->wait(1);
                $I->click($WPLegalpagesPro->excludeCountries_checkBox);
                $I->click($WPLegalpagesPro->sectionContinueBtn);

                $I->waitForText("This Website/Service is only intended for Users who do not qualify as Consumers, such as Business Users.",20);
                $I->see("This Website/Service is only intended for Users who do not qualify as Consumers, such as Business Users.");
                $I->waitForText("Users may not qualify as Consumers;",20);
                $I->see("Users may not qualify as Consumers;");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("This Website/Service is only intended for Users who do not qualify as Consumers, such as Business Users.",20);
                $I->see("This Website/Service is only intended for Users who do not qualify as Consumers, such as Business Users.");
                $I->waitForText("Users may not qualify as Consumers;",20);
                $I->see("Users may not qualify as Consumers;");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);
            }



            public function UserShouldSeeTermsandConditionsWizardCreatedAsPerContentRightsForAllRightsReserved(AcceptanceTester $I,
                                                                                                               Page\Acceptance\LoginPage $loginPage,
                                                                                                               Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {
                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);
                $I->wait(2);
                $I->click($WPLegalpagesPro->contentRightsBaner);
                $I->click($WPLegalpagesPro->allRightsReserved_radioBtn);
                $I->wait(1);
                $I->click($WPLegalpagesPro->addtionaDescriptionToexplain_checkBox);
                $I->click($WPLegalpagesPro->sectionContinueBtn);

                $I->waitForText("The Owner reserves and holds all intellectual property rights for any such content.",20);
                $I->see("The Owner reserves and holds all intellectual property rights for any such content.");
                $I->waitForText("In particular, but without limitation, Users may not broadcast, copy, save, download, print, share",20);
                $I->see("In particular, but without limitation, Users may not broadcast, copy, save, download, print, share");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("The Owner reserves and holds all intellectual property rights for any such content.",20);
                $I->see("The Owner reserves and holds all intellectual property rights for any such content.");
                $I->waitForText("In particular, but without limitation, Users may not broadcast, copy, save, download, print, share",20);
                $I->see("In particular, but without limitation, Users may not broadcast, copy, save, download, print, share");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);
            }


            public function UserShouldSeeTermsandConditionsWizardCreatedAsPerContentRightsForSomeRightsReserved(AcceptanceTester $I,
                                                                                                               Page\Acceptance\LoginPage $loginPage,
                                                                                                               Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {
                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);

                $I->click($WPLegalpagesPro->contentRightsBaner);
                $I->click($WPLegalpagesPro->someRightsReserved_radioBtn);
                $I->click($WPLegalpagesPro->sectionContinueBtn);

                $I->waitForText("Users can find further details about how they may use such content in the relevant section of this Website.",20);
                $I->see("Users can find further details about how they may use such content in the relevant section of this Website.");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("Users can find further details about how they may use such content in the relevant section of this Website.",20);
                $I->see("Users can find further details about how they may use such content in the relevant section of this Website.");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);
            }


            public function UserShouldSeeTermsandConditionsWizardCreatedAsPerCommonProvisions(AcceptanceTester $I,
                                                                                                                Page\Acceptance\LoginPage $loginPage,
                                                                                                                Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {

                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);

                $I->click($WPLegalpagesPro->commonProvisionsBaner);
                $I->click($WPLegalpagesPro->sectionContinueBtn);

                $I->waitForText("If required by applicable law,",20);
                $I->see("If required by applicable law,");
                $I->see("Should any provision of these Terms be or be deemed void,");
                $I->see("In case of failure to do so,");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("If required by applicable law,",20);
                $I->see("If required by applicable law,");
                $I->see("Should any provision of these Terms be or be deemed void,");
                $I->see("In case of failure to do so,");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);
            }



            public function UserShouldSeeTermsandConditionsWizardCreatedAsPerCommonProvisionswithSpecificallyIntegratedPrivacyPolicy(AcceptanceTester $I,
                                                                                              Page\Acceptance\LoginPage $loginPage,
                                                                                              Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {

                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);

                $I->click($WPLegalpagesPro->commonProvisionsBaner);
                $I->wait(1);
                $I->click($WPLegalpagesPro->specificallyIntegrateThePrivacyPloicy);
                $I->click($WPLegalpagesPro->ukUsers_CheckBox);
                $I->click($WPLegalpagesPro->usUsers_CheckBox);
                $I->click($WPLegalpagesPro->youNotifyPeopleOneMonth);
                $I->click($WPLegalpagesPro->suggestedWording);
                $I->wait(4);
                $I->click($WPLegalpagesPro->sectionContinueBtn);

                $I->waitForText("For information about the use of their personal data, Users must refer to the privacy policy of this Website which is hereby declared to be part of these Terms.",20);
                $I->see("For information about the use of their personal data, Users must refer to the privacy policy of this Website which is hereby declared to be part of these Terms.");
                $I->waitForText("If required by applicable law, the Owner will specify the date by which the modified Terms will enter into force.",20);
                $I->see("If required by applicable law, the Owner will specify the date by which the modified Terms will enter into force.");
                $I->waitForText("Should any provision of these Terms be or be deemed void, invalid or unenforceable, the parties shall do their best to find, in an amicable way, an agreement on valid and enforceable provisions thereby substituting the void, invalid or unenforceable parts.",20);
                $I->see("Should any provision of these Terms be or be deemed void, invalid or unenforceable, the parties shall do their best to find, in an amicable way, an agreement on valid and enforceable provisions thereby substituting the void, invalid or unenforceable parts.");
                $I->waitForText("Any such invalid or unenforceable provision will be interpreted, construed and reformed to the extent reasonably required to render it valid, enforceable and consistent with its original intent. These Terms constitute the entire Agreement between Users and the Owner with respect to the subject matter hereof, and supersede all other communications, including but not limited to all prior agreements, between the parties with respect to such subject matter. These Terms will be enforced to the fullest extent permitted by law.",20);
                $I->see("Any such invalid or unenforceable provision will be interpreted, construed and reformed to the extent reasonably required to render it valid, enforceable and consistent with its original intent. These Terms constitute the entire Agreement between Users and the Owner with respect to the subject matter hereof, and supersede all other communications, including but not limited to all prior agreements, between the parties with respect to such subject matter. These Terms will be enforced to the fullest extent permitted by law.");
                $I->waitForText("These Terms are governed by the law of the place where the Owner is based, as disclosed in the relevant section of this document, without regard to conflict of laws principles.",20);
                $I->see("These Terms are governed by the law of the place where the Owner is based, as disclosed in the relevant section of this document, without regard to conflict of laws principles.");
                $I->waitForText("The above does not apply to any Users that qualify as European Consumers, nor to Consumers based in Switzerland, Norway or Iceland.",20);
                $I->see("The above does not apply to any Users that qualify as European Consumers, nor to Consumers based in Switzerland, Norway or Iceland.");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("For information about the use of their personal data, Users must refer to the privacy policy of this Website which is hereby declared to be part of these Terms.",20);
                $I->see("For information about the use of their personal data, Users must refer to the privacy policy of this Website which is hereby declared to be part of these Terms.");
                $I->waitForText("If required by applicable law, the Owner will specify the date by which the modified Terms will enter into force.",20);
                $I->see("If required by applicable law, the Owner will specify the date by which the modified Terms will enter into force.");
                $I->waitForText("Should any provision of these Terms be or be deemed void, invalid or unenforceable, the parties shall do their best to find, in an amicable way, an agreement on valid and enforceable provisions thereby substituting the void, invalid or unenforceable parts.",20);
                $I->see("Should any provision of these Terms be or be deemed void, invalid or unenforceable, the parties shall do their best to find, in an amicable way, an agreement on valid and enforceable provisions thereby substituting the void, invalid or unenforceable parts.");
                $I->waitForText("Any such invalid or unenforceable provision will be interpreted, construed and reformed to the extent reasonably required to render it valid, enforceable and consistent with its original intent. These Terms constitute the entire Agreement between Users and the Owner with respect to the subject matter hereof, and supersede all other communications, including but not limited to all prior agreements, between the parties with respect to such subject matter. These Terms will be enforced to the fullest extent permitted by law.",20);
                $I->see("Any such invalid or unenforceable provision will be interpreted, construed and reformed to the extent reasonably required to render it valid, enforceable and consistent with its original intent. These Terms constitute the entire Agreement between Users and the Owner with respect to the subject matter hereof, and supersede all other communications, including but not limited to all prior agreements, between the parties with respect to such subject matter. These Terms will be enforced to the fullest extent permitted by law.");
                $I->waitForText("These Terms are governed by the law of the place where the Owner is based, as disclosed in the relevant section of this document, without regard to conflict of laws principles.",20);
                $I->see("These Terms are governed by the law of the place where the Owner is based, as disclosed in the relevant section of this document, without regard to conflict of laws principles.");
                $I->waitForText("The above does not apply to any Users that qualify as European Consumers, nor to Consumers based in Switzerland, Norway or Iceland.",20);
                $I->see("The above does not apply to any Users that qualify as European Consumers, nor to Consumers based in Switzerland, Norway or Iceland.");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);
            }


*/
            public function UserShouldSeeTermsandConditionsWizardCreatedAsPerAgeRestrictions(AcceptanceTester $I,
                                                                                              Page\Acceptance\LoginPage $loginPage,
                                                                                              Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {

                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);

                $I->click($WPLegalpagesPro->ageRestrictionsBanner);
                $I->click($WPLegalpagesPro->sectionContinueBtn);
                $I->wait(1);

                $I->waitForText("Age restriction on usage of this Website and the Service: To access and use this Website and any of its Service the User must be an adult under applicable law.",20);
                $I->see("Age restriction on usage of this Website and the Service: To access and use this Website and any of its Service the User must be an adult under applicable law.");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("Age restriction on usage of this Website and the Service: To access and use this Website and any of its Service the User must be an adult under applicable law.",20);
                $I->see("Age restriction on usage of this Website and the Service: To access and use this Website and any of its Service the User must be an adult under applicable law.");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);
            }


    /*
            public function UserShouldSeeTermsandConditionsWizardCreatedAsPerAgeRestrictionsWithAgeLimit(AcceptanceTester $I,
                                                                                              Page\Acceptance\LoginPage $loginPage,
                                                                                              Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {

                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);

                $I->click($WPLegalpagesPro->ageRestrictionsBanner);
                $I->click($WPLegalpagesPro->usersMayAccesTheApplicationEnterAge_radioBtn);
                $I->fillField($WPLegalpagesPro->setAgeLimitField,$WPLegalpagesPro->setAgeLimitValue);
                $I->click($WPLegalpagesPro->sectionContinueBtn);

                $I->waitForText("Usage of this Website/Service is age restricted,",20);
                $I->see("Usage of this Website/Service is age restricted,");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("Usage of this Website/Service is age restricted,",20);
                $I->see("Usage of this Website/Service is age restricted,");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);
            }


            public function UserShouldSeeTermsandConditionsWizardCreatedAsDisclaimersOfWarranties(AcceptanceTester $I,
                                                                                              Page\Acceptance\LoginPage $loginPage,
                                                                                              Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
            {

                $loginPage->userLogin($I);

                $I->seeElement($WPLegalpagesPro->legalPagesMenu);
                $I->click($WPLegalpagesPro->legalPagesMenu);

                $I->click($WPLegalpagesPro->wizardSubMenu);
                $I->waitForText("Terms and Conditions", 20);
                $I->see("Terms and Conditions");
                $I->wait(2);
                $I->click($WPLegalpagesPro->termsAndConditionsCreateBtn);

                $WPLegalpagesPro->termsAndConditionsFillFields($I);

                $I->click($WPLegalpagesPro->disclaimersOfWarrantiesBaner);
                $I->wait(1);
                $I->click($WPLegalpagesPro->addressEU_checkBox);
                $I->click($WPLegalpagesPro->suggestedAdditions_CheckBox);
                $I->click($WPLegalpagesPro->sectionContinueBtn);

                $I->waitForText("Nothing in these Terms creates any special relationship such as joint venture, employment, agency, or partnership between the involved parties.",20);
                $I->see("Nothing in these Terms creates any special relationship such as joint venture, employment, agency, or partnership between the involved parties.");
                $I->waitForText("The User agrees to indemnify and hold the Owner",20);
                $I->see("The User agrees to indemnify and hold the Owner");
                $I->waitForText("To the maximum extent permitted by applicable law, in no event shall the Owner",20);
                $I->see("To the maximum extent permitted by applicable law, in no event shall the Owner");

                $I->click($WPLegalpagesPro->createPageBtn);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
                $I->click($WPLegalpagesPro->publishPageBtn_1);
                $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
                $I->click($WPLegalpagesPro->publishPageBtn_2);

                $I->wait(2);
                $I->click($WPLegalpagesPro->wordPressBtn);
                $I->click($WPLegalpagesPro->termsAndConditionsText);

                $I->waitForText("Nothing in these Terms creates any special relationship such as joint venture, employment, agency, or partnership between the involved parties.",20);
                $I->see("Nothing in these Terms creates any special relationship such as joint venture, employment, agency, or partnership between the involved parties.");
                $I->waitForText("The User agrees to indemnify and hold the Owner",20);
                $I->see("The User agrees to indemnify and hold the Owner");
                $I->waitForText("To the maximum extent permitted by applicable law, in no event shall the Owner",20);
                $I->see("To the maximum extent permitted by applicable law, in no event shall the Owner");

                $WPLegalpagesPro->movePageToTrash($I);
                $loginPage->userLogout($I);

            }


*/

}