<?php

class PrivacyPolicyCest
{

    public function UserShouldSeePrivacyPolicyWizardCreatedAsPerInformationFilledByUser(AcceptanceTester $I,
                                                                                    Page\Acceptance\LoginPage $loginPage,
                                                                                    Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->wizardSubMenu);
        $I->waitForText("Privacy Policy", 20);
        $I->see("Privacy Policy");
        $I->wait(2);
        $I->click($WPLegalpagesPro->privacyPolicyCreateBtn);

        $WPLegalpagesPro->PrivacyPolicyFillFields($I);

        $I->click($WPLegalpagesPro->sectionContinueBtn);

        $I->waitForText("We use Personally Identifiable Information to customize the Website, to make appropriate service offerings, and to fulfill buying and selling requests on the Website.",20);
        $I->see("We use Personally Identifiable Information to customize the Website, to make appropriate service offerings, and to fulfill buying and selling requests on the Website.");
        $I->waitForText("Personally Identifiable Information collected by ".$WPLegalpagesPro->businessNameValue." is securely stored and is not accessible to third parties or employees of ".$WPLegalpagesPro->businessNameValue." except for use as indicated above.",20);
        $I->see("Personally Identifiable Information collected by ".$WPLegalpagesPro->businessNameValue." is securely stored and is not accessible to third parties or employees of ".$WPLegalpagesPro->businessNameValue." except for use as indicated above.");
        $I->waitForText($WPLegalpagesPro->businessNameValue." uses login information, including, but not limited to, IP addresses, ISPs, and browser types, to analyze trends, administer the Website, track a Users movement and use, and gather broad demographic information.",20);
        $I->see($WPLegalpagesPro->businessNameValue." uses login information, including, but not limited to, IP addresses, ISPs, and browser types, to analyze trends, administer the Website, track a Users movement and use, and gather broad demographic information.");
        $I->waitForText("We provide Users with a mechanism to delete/deactivate Personally Identifiable Information from the Website’s database by contacting.",20);
        $I->see("We provide Users with a mechanism to delete/deactivate Personally Identifiable Information from the Website’s database by contacting.");
        $I->waitForText($WPLegalpagesPro->domainNameValue." contains links to other websites.",20);
        $I->see($WPLegalpagesPro->domainNameValue." contains links to other websites.");

        $I->click($WPLegalpagesPro->createPageBtn);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
        $I->click($WPLegalpagesPro->publishPageBtn_1);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
        $I->click($WPLegalpagesPro->publishPageBtn_2);

        $I->wait(2);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->privacyPolicyPageText);

        $I->waitForText("We use Personally Identifiable Information to customize the Website, to make appropriate service offerings, and to fulfill buying and selling requests on the Website.",20);
        $I->see("We use Personally Identifiable Information to customize the Website, to make appropriate service offerings, and to fulfill buying and selling requests on the Website.");
        $I->waitForText("Personally Identifiable Information collected by ".$WPLegalpagesPro->businessNameValue." is securely stored and is not accessible to third parties or employees of ".$WPLegalpagesPro->businessNameValue." except for use as indicated above.",20);
        $I->see("Personally Identifiable Information collected by ".$WPLegalpagesPro->businessNameValue." is securely stored and is not accessible to third parties or employees of ".$WPLegalpagesPro->businessNameValue." except for use as indicated above.");
        $I->waitForText($WPLegalpagesPro->businessNameValue." uses login information, including, but not limited to, IP addresses, ISPs, and browser types, to analyze trends, administer the Website, track a Users movement and use, and gather broad demographic information.",20);
        $I->see($WPLegalpagesPro->businessNameValue." uses login information, including, but not limited to, IP addresses, ISPs, and browser types, to analyze trends, administer the Website, track a Users movement and use, and gather broad demographic information.");
        $I->waitForText("We provide Users with a mechanism to delete/deactivate Personally Identifiable Information from the Website’s database by contacting.",20);
        $I->see("We provide Users with a mechanism to delete/deactivate Personally Identifiable Information from the Website’s database by contacting.");
        $I->waitForText($WPLegalpagesPro->domainNameValue." contains links to other websites.",20);
        $I->see($WPLegalpagesPro->domainNameValue." contains links to other websites.");

        $WPLegalpagesPro->movePageToTrashForPrivacyPolicyWizard($I);
        $loginPage->userLogout($I);

    }


    public function UserShouldSeePrivacyPolicyWizardCreatedAsEveryCheckBoxSelectedAndRadioButtonsNotselectedByUser(AcceptanceTester $I,
                                                                                 Page\Acceptance\LoginPage $loginPage,
                                                                                 Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->wizardSubMenu);
        $I->waitForText("Privacy Policy", 20);
        $I->see("Privacy Policy");
        $I->wait(2);
        $I->click($WPLegalpagesPro->privacyPolicyCreateBtn);

        $WPLegalpagesPro->PrivacyPolicyFillFields($I);

        $I->click($WPLegalpagesPro->natureAndSizeOfTheBusiness_CheckBox);
        $I->click($WPLegalpagesPro->natureAndSizeOfTheAdvertisingInventory_CheckBox);
        $I->click($WPLegalpagesPro->byContactUs_CheckBox);
        $I->click($WPLegalpagesPro->byCotactUs_CheckBox);
        $I->click($WPLegalpagesPro->no_AllowThirdPartyService_radioBtn);
        $I->click($WPLegalpagesPro->no_ShareInformationWithThirdParty_radioBtn);
        $I->click($WPLegalpagesPro->no_DoYouUseCookies_radioBtn);
        $I->click($WPLegalpagesPro->no_implimentThirdPartyCookies_radioBtn);
        $I->click($WPLegalpagesPro->no_WebsiteContainLinksToOtherSites_radioBtn);
        $I->click($WPLegalpagesPro->no_Subpoena_radioBtn);
        $I->wait(5);
        $I->click($WPLegalpagesPro->sectionContinueBtn);

        $I->waitForText("We may collect basic user profile information from all of our Users.",20);
        $I->see("We may collect basic user profile information from all of our Users.");

        $I->click($WPLegalpagesPro->createPageBtn);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
        $I->click($WPLegalpagesPro->publishPageBtn_1);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
        $I->click($WPLegalpagesPro->publishPageBtn_2);

        $I->wait(2);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->privacyPolicyPageText);

        $I->waitForText("We may collect basic user profile information from all of our Users.",20);
        $I->see("We may collect basic user profile information from all of our Users.");

        $WPLegalpagesPro->movePageToTrashForPrivacyPolicyWizard($I);
        $loginPage->userLogout($I);

    }

}