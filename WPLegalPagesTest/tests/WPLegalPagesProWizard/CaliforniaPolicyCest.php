<?php

class CaliforniaPolicyCest
{

    public function UserShouldSeeCaliforniaPolicyWizardCreatedAsPerCookieAuditTable(AcceptanceTester $I,
                                                                                    Page\Acceptance\LoginPage $loginPage,
                                                                                    Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->wizardSubMenu);
        $I->waitForText("California Policy", 20);
        $I->see("California Policy");
        $I->wait(2);
        $I->click($WPLegalpagesPro->californiaPolicyWizardCrateBtn);

        $WPLegalpagesPro->CaliforniaPolicyFillFields($I);

        $I->click($WPLegalpagesPro->cookieAuditTableBaner);
        $I->click($WPLegalpagesPro->sectionContinueBtn);

        $I->waitForText("This Privacy Notice for California Residents supplements the information contained",20);
        $I->see("This Privacy Notice for California Residents supplements the information contained");

        $I->click($WPLegalpagesPro->createPageBtn);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
        $I->click($WPLegalpagesPro->publishPageBtn_1);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
        $I->click($WPLegalpagesPro->publishPageBtn_2);

        $I->wait(2);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->californiaPolicyPageText);

        $I->waitForText("This Privacy Notice for California Residents supplements the information contained",20);
        $I->see("This Privacy Notice for California Residents supplements the information contained");

        $WPLegalpagesPro->movePageToTrashForCCPAWizard($I);
        $loginPage->userLogout($I);

    }

    public function UserShouldSeeCaliforniaPolicyWizardCreatedAsPerInformationWebsiteCollects(AcceptanceTester $I,
                                                                                    Page\Acceptance\LoginPage $loginPage,
                                                                                    Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->wizardSubMenu);
        $I->waitForText("California Policy", 20);
        $I->see("California Policy");
        $I->wait(2);
        $I->click($WPLegalpagesPro->californiaPolicyWizardCrateBtn);

        $WPLegalpagesPro->CaliforniaPolicyFillFields($I);

        $I->click($WPLegalpagesPro->identifiers_CheckBox);
        $I->click($WPLegalpagesPro->personalInformationCategories_CheckBox);
        $I->click($WPLegalpagesPro->protectedClassificationCharacteristics_CheckBox);
        $I->click($WPLegalpagesPro->commercialInformation_CheckBox);
        $I->click($WPLegalpagesPro->biometricInformation_CheckBox);
        $I->click($WPLegalpagesPro->internetOrOtherSimilarNetworkActivities_checkBox);
        $I->click($WPLegalpagesPro->geolocationData_checkBox);
        $I->click($WPLegalpagesPro->sensoryData_checkBox);
        $I->click($WPLegalpagesPro->professionalOrEmployeeRelatedInformation_checkBox);
        $I->click($WPLegalpagesPro->nonPlublicEducationInformation_check_Box);
        $I->click($WPLegalpagesPro->inferencesDrawnFromOtherPersonalInformation_checkBox);
        $I->wait(1);
        $I->click($WPLegalpagesPro->sectionContinueBtn);

        $I->waitForText("A real name, alias, postal address, unique personal identifier, online identifier, Internet Protocol address, email address, account name, Social Security number, driver's license number, passport number, or other similar identifiers.",20);
        $I->see("A real name, alias, postal address, unique personal identifier, online identifier, Internet Protocol address, email address, account name, Social Security number, driver's license number, passport number, or other similar identifiers.");
        $I->waitForText("Personal information categories listed in the California Customer Records statute",20);
        $I->see("Personal information categories listed in the California Customer Records statute");
        $I->waitForText("Protected classification characteristics under California or federal law.",20);
        $I->see("Protected classification characteristics under California or federal law.");
        $I->waitForText("Commercial information.",20);
        $I->see("Commercial information.");
        $I->waitForText("Biometric information.",20);
        $I->see("Biometric information.");
        $I->waitForText("Internet or other similar network activity.",20);
        $I->see("Internet or other similar network activity.");
        $I->waitForText("Geolocation data.",20);
        $I->see("Geolocation data.");
        $I->waitForText("Professional or employment-related information.",20);
        $I->see("Professional or employment-related information.");
        $I->waitForText("Non-public education information",20);
        $I->see("Non-public education information");
        $I->waitForText("Inferences drawn from other personal information.",20);
        $I->see("Inferences drawn from other personal information.");

        $I->click($WPLegalpagesPro->createPageBtn);

        $I->waitForText("A real name, alias, postal address, unique personal identifier, online identifier, Internet Protocol address, email address, account name, Social Security number, driver's license number, passport number, or other similar identifiers.",20);
        $I->see("A real name, alias, postal address, unique personal identifier, online identifier, Internet Protocol address, email address, account name, Social Security number, driver's license number, passport number, or other similar identifiers.");
        $I->waitForText("Personal information categories listed in the California Customer Records statute",20);
        $I->see("Personal information categories listed in the California Customer Records statute");
        $I->waitForText("Protected classification characteristics under California or federal law.",20);
        $I->see("Protected classification characteristics under California or federal law.");
        $I->waitForText("Commercial information.",20);
        $I->see("Commercial information.");
        $I->waitForText("Biometric information.",20);
        $I->see("Biometric information.");
        $I->waitForText("Internet or other similar network activity.",20);
        $I->see("Internet or other similar network activity.");
        $I->waitForText("Geolocation data.",20);
        $I->see("Geolocation data.");
        $I->waitForText("Professional or employment-related information.",20);
        $I->see("Professional or employment-related information.");
        $I->waitForText("Non-public education information",20);
        $I->see("Non-public education information");
        $I->waitForText("Inferences drawn from other personal information.",20);
        $I->see("Inferences drawn from other personal information.");

        $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
        $I->click($WPLegalpagesPro->publishPageBtn_1);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
        $I->click($WPLegalpagesPro->publishPageBtn_2);

        $I->wait(2);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->californiaPolicyPageText);

        $WPLegalpagesPro->movePageToTrashForCCPAWizard($I);
        $loginPage->userLogout($I);

    }

}