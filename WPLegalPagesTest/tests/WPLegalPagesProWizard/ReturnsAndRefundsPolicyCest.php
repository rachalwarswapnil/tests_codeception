<?php

class ReturnsAndRefundsPolicyCest
{
    public function UserShouldSeeReturnsAndRefundsPolicyWizardCreatedAsPerInformationFilledByUser(AcceptanceTester $I,
                                                                                        Page\Acceptance\LoginPage $loginPage,
                                                                                        Page\WPLegalPagesPro\WPLegalpagesPro $WPLegalpagesPro)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalpagesPro->legalPagesMenu);
        $I->click($WPLegalpagesPro->legalPagesMenu);

        $I->click($WPLegalpagesPro->wizardSubMenu);
        $I->waitForText("Returns and Refunds Policy", 20);
        $I->see("Returns and Refunds Policy");
        $I->wait(2);
        $I->click($WPLegalpagesPro->returnsAndRefundsWizardsCreateBtn);

        $WPLegalpagesPro->ReturnsAndRefundsFillFields($I);

        $I->click($WPLegalpagesPro->sectionContinueBtn);

        $I->waitForText("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,20);
        $I->see("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("Contact our customer services department to get a free shipping label.",20);
        $I->see("Contact our customer services department to get a free shipping label.");

        $I->click($WPLegalpagesPro->createPageBtn);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_1);
        $I->click($WPLegalpagesPro->publishPageBtn_1);
        $I->waitForElement($WPLegalpagesPro->publishPageBtn_2);
        $I->click($WPLegalpagesPro->publishPageBtn_2);

        $I->wait(2);
        $I->click($WPLegalpagesPro->wordPressBtn);
        $I->click($WPLegalpagesPro->returnsAndRefundsPolicyPageText);

        $I->waitForText("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue,20);
        $I->see("Thanks for purchasing our products at ".$WPLegalpagesPro->domainNameValue." operated by ".$WPLegalpagesPro->businessNameValue);
        $I->waitForText("Contact our customer services department to get a free shipping label.",20);
        $I->see("Contact our customer services department to get a free shipping label.");

        $WPLegalpagesPro->movePageToTrashForReturnsAndRefundsWizard($I);
        $loginPage->userLogout($I);

    }

}