<?php
class TermsOfUseFRCest
{

    public function UserShouldSeeFrenchTermsOfUseTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                        Page\Acceptance\LoginPage $loginPage,
                                                                                        Page\WPLegalPages\WPLegalPages $WPLegalPages)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalPages->legalPagesMenu);
        $I->click($WPLegalPages->legalPagesMenu);

        $WPLegalPages->settingsFillFields($I);
        $I->click($WPLegalPages->saveBtn);
        $I->see($WPLegalPages->settingsSavedText);

        $I->click($WPLegalPages->createPageSubMenu);
        $I->click($WPLegalPages->selectFrenchLanguage);
        $I->click($WPLegalPages->termsOfUseFRPageCreateLink);
        $I->switchToIFrame($WPLegalPages->innerIframeId);

        $I->waitForText("produits et services disponibles sur le site " . $WPLegalPages->domainNameValue, 20);
        $I->see("produits et services disponibles sur le site " . $WPLegalPages->domainNameValue);
        $I->waitForText("titres et intérêts sur ces propriétés restent la propriété seule et unique de ".$WPLegalPages->businessNameValue,20);
        $I->see("titres et intérêts sur ces propriétés restent la propriété seule et unique de ".$WPLegalPages->businessNameValue);

        $I->switchToFrame();
        $I->click($WPLegalPages->publishBtn);

        $I->see($WPLegalPages->pageSuccesfullyCreated);
        $I->click($WPLegalPages->wordPressBtn);
        $I->click($WPLegalPages->FrenchTermsOfUseText);

        $I->waitForText("produits et services disponibles sur le site " . $WPLegalPages->domainNameValue, 20);
        $I->see("produits et services disponibles sur le site " . $WPLegalPages->domainNameValue);
        $I->waitForText("titres et intérêts sur ces propriétés restent la propriété seule et unique de ".$WPLegalPages->businessNameValue,20);
        $I->see("titres et intérêts sur ces propriétés restent la propriété seule et unique de ".$WPLegalPages->businessNameValue);

        $WPLegalPages->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}