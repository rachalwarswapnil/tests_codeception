<?php

class TermsOfUseDECest
{
    public function UserShouldSeeGermanTermsOfUseTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                            Page\Acceptance\LoginPage $loginPage,
                                                                                            Page\WPLegalPages\WPLegalPages $WPLegalPages)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalPages->legalPagesMenu);
        $I->click($WPLegalPages->legalPagesMenu);

        $WPLegalPages->settingsFillFields($I);
        $I->click($WPLegalPages->saveBtn);
        $I->see($WPLegalPages->settingsSavedText);

        $I->click($WPLegalPages->createPageSubMenu);
        $I->click($WPLegalPages->selectGermanLanguage);
        $I->click($WPLegalPages->termsOfUseGermanPageCreateLink);
        $I->switchToIFrame($WPLegalPages->innerIframeId);

        $I->waitForText("die auf der von ".$WPLegalPages->businessNameValue,20);
        $I->see("die auf der von ".$WPLegalPages->businessNameValue);
        $I->waitForText("betriebenen Website ".$WPLegalPages->domainNameValue,20);
        $I->see("betriebenen Website ".$WPLegalPages->domainNameValue);

        $I->switchToFrame();
        $I->click($WPLegalPages->publishBtn);

        $I->see($WPLegalPages->pageSuccesfullyCreated);
        $I->click($WPLegalPages->wordPressBtn);
        $I->click($WPLegalPages->GermanTermsOfUseText);

        $I->waitForText("die auf der von ".$WPLegalPages->businessNameValue,20);
        $I->see("die auf der von ".$WPLegalPages->businessNameValue);
        $I->waitForText("betriebenen Website ".$WPLegalPages->domainNameValue,20);
        $I->see("betriebenen Website ".$WPLegalPages->domainNameValue);

        $WPLegalPages->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}