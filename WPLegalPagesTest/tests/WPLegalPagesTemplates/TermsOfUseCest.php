<?php

class TermsOfUseCest
{
    public function UserShouldSeeTermsOfUseTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                         Page\Acceptance\LoginPage $loginPage,
                                                                                      Page\WPLegalPages\WPLegalPages $WPLegalPages)
    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalPages->legalPagesMenu);
        $I->click($WPLegalPages->legalPagesMenu);

        $WPLegalPages->settingsFillFields($I);
        $I->click($WPLegalPages->saveBtn);
        $I->see($WPLegalPages->settingsSavedText);

        $I->click($WPLegalPages->createPageSubMenu);
        $I->click($WPLegalPages->termsOfUsePageCreateLink);
        $I->switchToIFrame($WPLegalPages->innerIframeId);

        $I->waitForText("Services available at ".$WPLegalPages->domainNameValue." website",30);
        $I->see("Services available at ".$WPLegalPages->domainNameValue." website");
        $I->waitForText("operated by ".$WPLegalPages->businessNameValue,30);
        $I->see("operated by ".$WPLegalPages->businessNameValue);
        $I->waitForText("solely with ".$WPLegalPages->businessNameValue." and its licensors.",30);
        $I->see("solely with ".$WPLegalPages->businessNameValue." and its licensors.");
        $I->waitForText("Neither ".$WPLegalPages->businessNameValue.", nor its suppliers and licensors, makes any warranty that our Services will be error free",30);

        $I->switchToFrame();
        $I->click($WPLegalPages->publishBtn);

        $I->see($WPLegalPages->pageSuccesfullyCreated);
        $I->click($WPLegalPages->wordPressBtn);
        $I->click($WPLegalPages->termsOfUseText);

        $I->waitForText("Services available at ".$WPLegalPages->domainNameValue." website",30);
        $I->see("Services available at ".$WPLegalPages->domainNameValue." website");
        $I->waitForText("operated by ".$WPLegalPages->businessNameValue,30);
        $I->see("operated by ".$WPLegalPages->businessNameValue);
        $I->waitForText("solely with ".$WPLegalPages->businessNameValue." and its licensors.",30);
        $I->see("solely with ".$WPLegalPages->businessNameValue." and its licensors.");
        $I->waitForText("Neither ".$WPLegalPages->businessNameValue.", nor its suppliers and licensors, makes any warranty that our Services will be error free",30);

        $WPLegalPages->moveTemplateToTrash($I);
        $loginPage->userLogout($I);

    }
}