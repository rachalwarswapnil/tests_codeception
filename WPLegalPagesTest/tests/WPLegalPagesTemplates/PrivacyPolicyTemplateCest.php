<?php

class PrivacyPolicyTemplateCest
{
    public function UserShouldSeePrivacyPolicyTemplateCreatedAsPerInformationFilledByHim(AcceptanceTester $I,
                                                                                         Page\Acceptance\LoginPage $loginPage,
                                                                                         Page\WPLegalPages\WPLegalPages $WPLegalPages)

    {
        $loginPage->userLogin($I);

        $I->seeElement($WPLegalPages->legalPagesMenu);
        $I->click($WPLegalPages->legalPagesMenu);

        $WPLegalPages->settingsFillFields($I);
        $I->click($WPLegalPages->saveBtn);
        $I->see($WPLegalPages->settingsSavedText);

        $I->click($WPLegalPages->createPageSubMenu);
        $I->click($WPLegalPages->privacyPolicyPageCreateLink);

        $I->switchToIFrame($WPLegalPages->innerIframeId);
        $I->waitForText("Welcome to ".$WPLegalPages->domainNameValue,30);
        $I->see("offered by ".$WPLegalPages->businessNameValue);

        $I->switchToFrame();
        $I->click($WPLegalPages->publishBtn);

        $I->see($WPLegalPages->pageSuccesfullyCreated);
        $I->click($WPLegalPages->wordPressBtn);
        $I->click($WPLegalPages->privacyPolicyText);

        $I->see("Welcome to ".$WPLegalPages->domainNameValue);
        $I->see("emailing us at ".$WPLegalPages->emailAddressValue);

        $WPLegalPages->moveTemplateToTrash($I);
        $loginPage->userLogout($I);
    }
}